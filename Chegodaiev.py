"""
CPoulard  ; mai 2021
côté "métier" : montrer le principe des formules Tchégodaiev / Hazen utilisées en hydrologie (TD 1ere année ENTPE)  et la relation fréquence/période de retour
coté "Python" : mettre en oeuvre deux widgets de matplotlib (Checkbutton et Slider), faciles d'emploi mais au rendu pas très abouti ; définir les fonctions associées à des événements sur ces boutons ; manipuler les fonctions en tant qu'objet (fonction_en_cours = une des  3 fonctions disponibles...)
"""


from matplotlib import pyplot as plt
from matplotlib.widgets import Slider, CheckButtons  # widgets du module matplotlib !
import numpy as np
from numpy.random import gumbel

# FONCTIONS

# trois fonctions correspondant chacune à une manière de définir un échantillon de nb valeurs
def echantillon_uniforme(nb):
    return sorted(range(1, nb+1), reverse=True)

def echantillon_constant(nb):
    # non encore testé
    return [1]*nb

def echantillon_gumbel(nb):
    # non encore testé
    x0 = 196
    gr = 102

    return sorted(gumbel(loc=x0, scale=gr, size=nb))

# deux fonctions correspondant chacune à une manière de définir une période de retour empirique
# on pourrait se contenter de la seconde, avec des paramètres par défaut égaux à ceux de la première

def T_emp_Tchego(n_annees):

    plotting_positions_Tchego = [((n_annees + 0.4) / ((indice + 1) - 0.3)) for indice in range(n_annees)]
    print("Tchégo ", plotting_positions_Tchego)
    # amélioration de l'affichage avec le méthode join
    print("Périodes de retour empiriques TTchégo : ", " / ".join([f"{T:.2f}" for T in plotting_positions_Tchego]))
    return plotting_positions_Tchego

def T_emp_parametre(n_annees, a , b):

    plotting_positions_param  = [((n_annees + b) / ((indice + 1) - a)) for indice in range(n_annees)]
    print("Paramétré par curseur ", plotting_positions_param)
    return plotting_positions_param


def update_vlines(courbe_en_vlines, x, ymin=None, ymax=None):
    # mise à jour de courbes de type vlines
    # https://stackoverflow.com/questions/29331401/updating-pyplot-vlines-in-interactive-plot
    seg_old = courbe_en_vlines.get_segments()
    if ymin is None:
        ymin = seg_old[0][0, 1]
    if ymax is None:
        ymax = seg_old[0][1, 1]

    seg_new = [np.array([[xx, ymin],
                         [xx, ymax]]) for xx in x]

    courbe_en_vlines.set_segments(seg_new)

def retracer_Tchego(echantillon):
    """
    on met à jour la courbe en vlines grâce à la fonction update_vlines ci-dessus
    """
    y_max = max(echantillon)
    T_emp = T_emp_Tchego(len(echantillon))

    if en_periode_de_retour:
        update_vlines(courbe_estim_Tchego,  [T for T in T_emp], ymin=0, ymax=y_max)
    else:
        update_vlines(courbe_estim_Tchego, [1 - (1 / T) for T in T_emp], ymin=0, ymax=y_max)

    # pas d'appel à redessiner le canevas car si cette fontion est appelée elle est toujours suivie de retracer_parametree


def retracer_parametree(echantillon, a, b):

    T_emp = T_emp_parametre(len(echantillon), a,b)

    if en_periode_de_retour:
        courbe_estim_param.set_data(T_emp, echantillon)
        ax.set_title(f"Estimations de T empirique\n de Tchegodayev et paramétrée a={a:.2f} et b={b:.2f} ")
        ax.set_xlim(xmin=min(T_emp), xmax=max(T_emp) * 1.1)
    else:
        freq = [ 1 - (1 / T) for T in T_emp]
        courbe_estim_param.set_data(freq, echantillon)
        ax.set_title(f"Estimations de la fréquence empirique\n de Tchegodayev et paramétrée a={a:.2f} et b={b:.2f} ")
        ax.set_xlim(xmin=min(freq), xmax=max(freq) * 1.1)
    #ax.set_xbounds(lower=min(T_emp), upper=max(T_emp))

    fig_pp.canvas.draw_idle()

# https://matplotlib.org/stable/gallery/widgets/slider_demo.html
# https://stackoverflow.com/questions/13656387/can-i-make-matplotlib-sliders-more-discrete
def update_a(val):
    global a
    a = val
    retracer_parametree(echantillon, a, b)

def update_b(val):
    global b
    b = val
    retracer_parametree(echantillon, a, b)

def update_N(val):
    global N, echantillon
    N = int( val)  # ou bien    N = slider_a.val
    echantillon = methode_echantillonnage(N)

    retracer_Tchego(echantillon)
    retracer_parametree(echantillon, a, b)
    ax.set_ylim(bottom=0, top=N*1.1)
    fig_pp.canvas.draw_idle()

def switch_freq(label):
    global en_periode_de_retour

    en_periode_de_retour = not en_periode_de_retour
    retracer_parametree(echantillon, a, b)
    retracer_Tchego(echantillon)
    #ax.set_ylim(bottom=0, top=N * 1.1)

    fig_pp.canvas.draw_idle()

#plt.ion()


a_Tchego, b_Tchego = 0.3 , 0.4  # affectation de plusieurs valeurs simultanées par tuples
# initialisation des paramètres de départ (modifiables par curseur)
a = a_Tchego
b = b_Tchego
N = 10
methode_echantillonnage = echantillon_uniforme
echantillon = methode_echantillonnage(N)
en_periode_de_retour = True

print(echantillon)
print(a, b)


fig_pp, (ax, ax_slide_a, ax_slide_b, ax_slide_n, ax_chb) = plt.subplots(nrows=5, ncols=1, gridspec_kw={'height_ratios': [10, 1, 1, 1,2]})
plt.subplots_adjust(wspace=1, hspace=0.5,left=0.1,top=0.85,right=0.9,bottom=0.1)
fig_pp.canvas.set_window_title("Démo périodes de retour 'empiriques'")

fig_pp.suptitle(f"seul le rang des observations est utilisé dans le calcul des T_empiriques ! ")

ax.set_xlabel("Période de retour (années)")

courbe_estim_Tchego = ax.vlines(x=T_emp_Tchego(N), ymin=0, ymax=max(echantillon), color="red", alpha=0.7, linewidth=2,
                                 zorder=2, label= "Tchegodaiev", ls="--")
courbe_estim_param, =  ax.plot(T_emp_parametre(N, a, b), echantillon, color='blue', alpha=0.7, linewidth=5, solid_capstyle='round',
                                    zorder=10, label = "paramétrée", marker="x" , ls=':', markersize=7)


for ax_s in [ax_slide_a, ax_slide_b, ax_slide_n, ax_chb]:
    ax_s.xaxis.set_visible(False)
    ax_s.yaxis.set_visible(False)

for pos in ['right', 'top', 'bottom', 'left']:
    ax_chb.spines[pos].set_visible(False)

print("ici")

# nom connu même hors de la fonction pour éviter le GC ?
# a
slider_a = Slider(
    ax_slide_a, "paramètre a ", valmin=0.01, valmax = 0.99, valfmt='%0.2f', valinit=a, color="green")

slider_a.on_changed(update_a)

# b
slider_b = Slider(
    ax_slide_b, "paramètre b ", valmin=0.01, valmax = 0.99, valfmt='%0.2f', valinit=b, color="blue")

slider_b.on_changed(update_b)

# N
valeurs_possibles_N = np.linspace(0, 100, 1)
slider_n = Slider(
    ax_slide_n, "paramètre N ", valmin=5, valmax = 200, valfmt='%0.0f', valinit=N, valstep=valeurs_possibles_N, color="purple")

slider_n.on_changed(update_N)

# switch T / freq

chb_enT = CheckButtons(ax_chb, [ "Période de retour (années) ; sinon fréquence (de 0 à 1)"], [False])
"""
for rect in chb_enT.rectangles:
    rect.set_width(0.1)
    rect.set_height(1)
    rect.xy = (0.05,0)
"""

chb_enT.on_clicked(switch_freq)


ax.legend(title="formules", bbox_to_anchor=(1.05, 0.10), loc='lower right')
#plt.tight_layout()
#fig_pp.savefig("ma_figure.png")
#fig_pp.canvas.draw()
plt.show()
#plt.ioff()
