# Christine Poulard, Oct 2021
# atelier D, Cartes

from matplotlib import pyplot as plt
from matplotlib.patches import Rectangle

ma_matrice = [[1, 2, 30, 50, 120], [21, 2, 50, 50, 90], [-1, 2, -30, 50, -120], [41, 42, 80, 50, 50], [9, 29, 39, 590, 10]]

fig = plt.figure("D'une matrice à une carte de champ (matshow)")
ax = fig.add_subplot(111)
ax.set_title("défaut : centres des pixels \n sur les points de grille")
# on a ajouté une colorbar sans spécifier d'arguments


cax = ax.matshow(ma_matrice)
# on définit un rectangle puis on l'ajoute à cax pour se repérer
case_en_haut_a_gauche = Rectangle((0, 0), width=1, height=1, lw=2, edgecolor='orange',
                                  facecolor='None')

ax.add_patch(case_en_haut_a_gauche)
# deux instructions pour définir la représentation de l'échelle colorimétrique et son titre
cbar = fig.colorbar(cax, ax=ax, extend='both')
cbar.set_label('colorbar par défaut ')
plt.tick_params(labeltop=False, labelbottom=True)
plt.show()

fig = plt.figure("D'une matrice à une carte de champ rectangulaire (pcolormesh)")
ax = fig.add_subplot(111)
ax.set_title("mailles rectangulaires définies \n par vecteurs x et y")

vecteur_x=[0,1,5,10,11,15]  # un de plus que le nombre de colonnes de la matrice
vecteur_y=[0,2,5,10,15,20] # un de plus que le nombre de lignes de la matrice
cmax = ax.pcolormesh(vecteur_x, vecteur_y, ma_matrice)
# on définit un rectangle puis on l'ajoute à cax pour se repérer
case_en_haut_a_gauche = Rectangle((0, 0), width=1, height=2, lw=2, edgecolor='orange',
                                  facecolor='None')

ax.add_patch(case_en_haut_a_gauche)
cbar = fig.colorbar(cmax, ax=ax, extend='both')
cbar.set_label('colorbar par défaut ')
plt.tick_params(labeltop=False, labelbottom=True)
plt.show()



fig = plt.figure("D'une matrice à des isolignes (contourf)")
ax = fig.add_subplot(111)
ax.set_title("plages séparées par des \n isolignes")
vecteur_x=[0.5,3,7.5,10.5,13]  # = nombre de colonnes de la matrice
vecteur_y=[1,3.5,7.5,12.5,17.5] # = nombre de lignes de la matrice
ccax = ax.contourf(vecteur_x, vecteur_y, ma_matrice)
# on définit un rectangle puis on l'ajoute à cax pour se repérer
case_en_haut_a_gauche = Rectangle((0, 0), width=1, height=2, lw=2, edgecolor='orange',
                                  facecolor='None')

ax.add_patch(case_en_haut_a_gauche)
cbar = fig.colorbar(ccax, ax=ax, extend='both')
cbar.set_label('colorbar par défaut ')
plt.tick_params(labeltop=False, labelbottom=True)
plt.show()
