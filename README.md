## Vademecum Python, avec focus sur MatPlotLib

Les **bases en Python et quelques astuces** pour bien démarrer avec Matplotlib, et une **collection d'exemples en hydrologie et hydraulique**, mis en forme et expliqués dans le [wiki de ce projet](https://gitlab.irstea.fr/christine.poulard/atelier-matplotlib/-/wikis/home), parce que...
- on a toujours besoin de visualiser ses données et ses résultats
- il est parfois très chronophage de trouver certaines informations, même si la question a l'air anodine (problème de mot-clé, question atypique, mauvaise piste...) ; une fois la solution trouvée, il est prudent d'en garder une trace ; autant partager !
- réaliser quelques figures est aussi un bon moyen d'entrer dans Python en manipulant ses concepts


## Vademecum Python
Quelques pages rappellent les **bases de Python**, initialement les bases utiles pour les graphiques, à l'attention de persnnes qui n'ont pas de gros bagage en Python mais souhaitent réaliser des graphes.
Au fur et à mesure, on ajoute des informations plus avancées:
- créer des environnements virtuels pour gérer les versions de Python et les versions des modules importés (cet aspect devient rapidement important !) : venv, poetry
- à venir : distribuer un code (zippap, ...)
- bien choisir ses types et les préciser (type hints et docstring suivant une convention) : types de Collections, de dataclass, ...
- les modules utiles en phase de développement, rencontrés dans des formations (profiling, gestion des warnings et erreurs, ... )

## Les interfaces graphiques
La partie "Atelier Python" vous propose de rendre **interactives** des fenêtres Matplotlib avec des fonctions et widgets du module Matplotlib. Cela permet d'avoir un résultat en un temps raisonnable et de découvrir les grands principes (associer une action à une interaction avec un widget ou un objet rendu réceptif).
Pour aller plus loin :
- **Tkinter** : facile à prendre en main, et présent dans la bibliothèque standard, il connaît toutefois des limitations. C'est le module utilisé pour mes projets **ST2shape** et les boîtes à outils hydrologiques. Il est utile de connaître le module **filedialog** qui permet, même en l'absence d'interface graphique, de proposer des fenêtres de dialogues pour ouvrir un fichier ou définir un chemin d'accès pour sauvegarder un fichier.
- **PyQT** : PyQT est basé sur "QT", "ZE" outil en C pour réaliser des interfaces, qui est donc très largement utilisé. Une page résume la présentation de Sylvain Coulibaly, développeur de "Pamhyr en Python" (2002) : utiliser **QTDesigner** pour créer une interface, connaître les types d'objets et leurs propriétés, la convertir en Python, associer des actions à aux widgets, définir des **signaux** (QTSignal), insérer un graphique MatplotLib, internationaliser avex **QTLinguist**.

## Atelier Matplotlib
**Avertissement ** : La plupart des codes ont été écrits avec une version 3.4. de matplotlib  **la version actuelle est 3.8 ** (avril 2024) !


**Galerie de quelques exemples**  Voir le tableau dans la page d'accueil du wiki pour la correspondance Atelier / Illustration / notions abordées, et la Galerie pour des exemples complémentaires.

<table>
  <tr>
     <td>
        <figure>
          <img src="/illustrations/Figure_3_graphique_PTQ_subplots_avec-legende.png"  width="240" > 
          <figcaption align = "center"><i>graphiques simples, puis chroniques, puis plusieurs axes des y ; légende de figure, légende customisée (exemple construit sur la base d'une appli réalisée pour un TD)</i></figcaption></figure>
        </td>
     <td>
       <figure>
         <img src="/illustrations/carte.png"  width="240">
         <figcaption align = "center"><i>carte à partir d'une matrice et de shapes, colorbar customisée avec une liste de couleurs et de bornes ; existe aussi en version "colorbar continue discrétisée" (exemple construit à partie de tests pour customiser une colorbar)</i></figcaption>
         </figure> </td>
     <td>  
         <figure>
         <img src="/images/DixAnsAvecCentennaleEtQuinquennale.png"  width="240">
         <figcaption align = "center"><i>légende customisée, interactions avec sliders ; ajout d'outils à la barre d'outils (étape de construction d'un outil pédagogique pour un TD)</i></figcaption>
         </figure></td>    
   </tr> 
   <tr>
      <td><figure>
      <img src="/illustrations/InterfaceCourbeDeRemous2.png"  width="240">
         <figcaption align = "center"><i>coloration d'une zone avec fill_between, interactions avec sliders ; la légende est reportée dans une vignette dédiée (application interactive à partir d'un code de S. Berni pour un TD d'hydraulique)</i>
         </figcaption></figure></td>  
      <td><figure><img src="illustrations/selection_picker.png"  width="240"> 
        <figcaption align = "center"><i>sélection d'un point au clic de souris, calcul distances écran, annotations et mise à jour d'annotations (tests pour un prototype de l'application Pamhyr en Python, avec l'équipe hydraulique)</i>
        </figcaption></figure></td>
      <td><figure><img src="illustrations/PresentationVisuSerieDynamique.png"  width="240"> 
        <figcaption align = "center"><i>prétraitement : adaptation de la résolution à la taille de la fenêtre, et surcharge du zoom (tests pour améliorer le tracé de longues chroniques)</i>
        </figcaption></figure></td>
       
  </tr>
</table>

Les codes sont commentés dans le [WIKI ASSOCIE](https://gitlab.irstea.fr/christine.poulard/atelier-matplotlib/-/wikis/home), pour aider à prendre en main MatPlobLib à travers des exemples de tracé de chronique, de cartes, et création de figures interactives. 
Les exercices sont progressifs, proposant s'améliorer la figure d'étape en étape, pour permettre à des lecteurs de différents niveaux d'y trouver leur compte.
Quelques pages font le point sur les bases utiles de Python et proposent des références intéressantes.

**Auteurs:** 

Christine Poulard 
Autres contributeurs et relecteurs bienvenus !

**Email:** 

*christine.poulard@inrae.fr*

**Contexte:**



Références : 


** License:**

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
