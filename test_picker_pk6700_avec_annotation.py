
"""
Atelier Matplotlib, C. Poulard, sept 2021
 code picker avec calcul des distances en unités "des données" et "écran"
 Réf : # https://matplotlib.org/stable/users/event_handling.html#object-picking


"""


import numpy as np
import matplotlib.pyplot as plt

# on crée des objets qui seront connus depuis la fonction ; ils seront plus précisément définis dans le "main"
adnotacja = None
adnotacja_ecran = None
une_croix = None
carre_jaune=None

def onpick_cp(event):
    """
    # on récupère un "événement " qui est un clic de souris et compare sa position aves les points d'un objet matplotlib
    # cette fonction sera appelée pour chaque "artist" pour lequel le picker a été activé, ici une ligne
    : arguments : un événement déclencheur, avec ses attributs :
       #  event.mouseevent : les caractéristiques du clic de souris, avec comme arguments la position (xdata , ydata)
       #  event.artist : la ligne courante, parmi celles pour qui le pickler est activé
       #  event.ind : les indices des points de la ligne compris dans la portée du pickler

    """

    # caractéristiques de event.artist : l' "artist" est ici une ligne :
    thisline = event.artist
    xdata = thisline.get_xdata()
    ydata = thisline.get_ydata()
    etiq = thisline.get_label()
    print(type(thisline), etiq)

    # caractéristiques de event.mouseevent, un clic de souris :
    x_souris, y_souris = event.mouseevent.xdata, event.mouseevent.ydata
    # print("position  de la souris sur le canevas en pixels", event.mouseevent.x, event.mouseevent.y)
    print("position  de la souris, en coordonnées de la courbe : ", x_souris, y_souris )
    # on déplace la croix vers le point cliqué
    une_croix.set_data([event.mouseevent.xdata], [event.mouseevent.ydata])

    # caractéristiques du croisement mouseevent + artist  : points dans la zone définie par le rayon
    # indices des points de l'artist sélectionnés
    ind = event.ind
    print("indices des points de la ligne sélectionnés par le clic: ", ind)
    # on en déduit la liste des coordonnées (x,y) des points sélectionnés
    points = np.array([(xdata[i], ydata[i]) for i in ind])
    print(" points sélectionnés par l'événement :", points)

    # premier calcul : distances "en unités des courbes"
    # calcul des distances entre le vecteur des points sélectionnés et le vecteur des coordonnées de la souris
    distances = np.linalg.norm(points - [(x_souris, y_souris)], axis=1)
    dataidx = np.argmin(distances)   # indice de la distance minimale
    datapos = points[dataidx,:]      # coordonnées du point d'indice de la distance minimale
    
    print('coordonnées du point le plus proche:', datapos)

    # on déplace la 1ère annotation vers le point cliqué et on met à jour son contenu, avec comme texte la distance
    adnotacja.set_text ( f"dist en m {distances[dataidx]:.2f}")
    adnotacja.set_position( (x_souris+20, y_souris+0.5))
    adnotacja.xy = datapos
    adnotacja.set_color(thisline.get_color())
    adnotacja.set_visible(True)

    # deuxième calculn en unités écran
    # on calcule la hauteur et la largeur courante de la vignette en unités "des courbes"
    xdroite, xgauche = ax.get_xlim()
    ybas, yhaut = ax.get_ylim()
    delta_x = abs(xdroite - xgauche)
    delta_y = abs(ybas - yhaut)
    # on récupère la hauteur et la largeur courante de la vignette en unités "écrans", càd pixels si ensuite multiplié par fig.dpi

    bbox = ax.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
    ratio_w_sur_h = bbox.width / bbox.height  # le graphique est "ratio_w_sur_h fois plus large que haut"
    print("delta_x, delta_y, ratio_w_sur_h)", delta_x, delta_y, ratio_w_sur_h)

    distances_normees = [(((x - x_souris) / delta_x ) ** 2 +
                ((y - y_souris) / delta_y)**2) ** (1 / 2) for (x, y) in points]
    distances_ecran = [((ratio_w_sur_h * (x -x_souris) / delta_x ) ** 2 +
                ((y - y_souris) / delta_y)**2) ** (1 / 2) for (x, y) in points]
    print("liste des distances normees", distances_normees)
    print("liste des distances écran", distances_ecran)
    dataidx_ecran = np.argmin(distances_ecran)
    datapos_ecran = points[dataidx_ecran, :]

    xpt_ecran, ypt_ecran = datapos_ecran

    # on déplace la 2e annotation vers le point cliqué et on met à jour son contenu, avec comme texte la distance "en unité écran"
    adnotacja_ecran.set_text ( f"dist écran : {distances_ecran[dataidx]:.2f}")
    adnotacja_ecran.set_position( (xpt_ecran-150, ypt_ecran-1.5))
    adnotacja_ecran.xy = (xpt_ecran, ypt_ecran)
    adnotacja_ecran.set_visible(True)
    # on déplace le carré jaune qui indique le pont sélectionné par cette méthode
    carre_jaune.set_data((xpt_ecran, ypt_ecran))
    
    # mise à jour de l'affichage
    plt.draw()
     
if __name__ == "__main__":
    # données (section en travers d'un cours d'eau)
    x_sc = [-240.838,-80.39,-78.15,-34,-20.91,-17.27,0,4.51,6.76,8.76,14.76,42.76,46.76,57.76,74.76,78.62,83.32,86.85,104,136.22,143.85,262.877]
    z_sc=[142.75,139,140,139.17,139.17,138.47,137.8,136.73,135.21,135.01,134.71,134.51,134.61,134.29,134.71,135.83,138.48,137.77,139.53,140.68,141.5,141.91]

    # on définit une figure
    fig, ax = plt.subplots()
    ax.set_title("Tests de l'événement pickler : click on points")
    line_pk6700, = ax.plot(x_sc, z_sc, '*', c='red', label="rouge", picker=20)
                    # picker=False,   # 5 points tolerance pickradius=5,

    #line, = ax.plot(np.random.rand(100), 'o', label="bleue",
    #               picker=True, pickradius=5)  # 5 points tolerance

    # on initialise les objets qui vont servir à se repérer  :
    # croix = localisation du clic de souris
    #  annotations pour les résultats en unité de la courbe puis en unité écran (au début, elles seront cachées)
    #  carré jaune pour le point sélectionné par la méthode en coordonnées écran (au début, ce sera le premier point)
    minimum_des_x = min(line_pk6700.get_xdata())
    minimum_des_y = min(line_pk6700.get_ydata())

    une_croix, = ax.plot(minimum_des_x, minimum_des_y, 'x', c='green')
    carre_jaune, = ax.plot(x_sc[0], z_sc[0], 's', c='yellow', alpha=0.5, markersize=20)

    adnotacja = ax.annotate("X", xytext=(minimum_des_x,minimum_des_y), xy=(minimum_des_x, minimum_des_y), bbox=dict(boxstyle='round,pad=0.5', fc='lightblue', alpha=0.75), arrowprops = dict(arrowstyle='->', connectionstyle='arc3,rad=0', color='grey'))
    adnotacja.set_color("blue")
    adnotacja.set_visible(False)
    adnotacja_ecran = ax.annotate("X", xytext=(minimum_des_x,minimum_des_y), xy=(minimum_des_x, minimum_des_y), bbox=dict(boxstyle='round,pad=0.5', fc='yellow', alpha=0.75), arrowprops = dict(arrowstyle='->', connectionstyle='arc3,rad=0', color='grey'))
    adnotacja_ecran.set_color("red")
    adnotacja_ecran.set_visible(False)
    fig.canvas.mpl_connect('pick_event', onpick_cp)
    plt.show()

