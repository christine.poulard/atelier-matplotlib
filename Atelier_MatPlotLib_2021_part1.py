# Christine Poulard, mars 2021
# Atelier d'initiation à MatplotLib

"""
# LES IMPORTS   -    jamais d'import avec étoile !
import mon_module                                 # ensuite on appellera les fonctions du module avec mon_module.une_fonction
                                                 # mon_module = espace de nommage
import mon_module_avec_un_nom_complique as momo  # ALIAS :  ensuite on appellera les fonctions avec momo.une_fonction
from mon_module import une_fonction_utile, une_autre_fonction_utile
                                                # import sélectif : je peux appeler une_fonction_utile (pas besoin de préciser l'espace de nommage
                                                #risque  de confusion (exemple d'un objet Polygon, avec un nom commun à plusieurs modules)
from mon_module_Truc import une_fonction_utile as truc_fu # la fonction a un alias que je choisis de manière à être simple mais non ambigu
"""

# la ligne "de base" pour travailler avec matplotlib.pyplot, avec l'alias plt par convention
import matplotlib.pyplot as plt

# des éléments complémentaires (on verra leur utilité)
import matplotlib.gridspec as gridspec
import matplotlib.dates as mdates # https://matplotlib.org/examples/api/date_demo.html
from matplotlib.dates import DateFormatter
import matplotlib.colors as mocolor
from matplotlib.patches import Rectangle
from matplotlib.lines import Line2D
from matplotlib.colors import ListedColormap
from matplotlib.collections import PatchCollection

# gestion des fichiers et répertoires
import os
from pathlib import Path

# une astuce pour aller chercher un fichier, même sans construire d'interface
from tkinter.filedialog import askopenfilename

# le module "standard" de gestion des dates
from datetime import datetime, timedelta

# le module de DataFrames
import pandas as pd

# pour travailler avec de "vrais" vecteurs et matrices typées (calcul numérique performant)
import numpy as np

# un prétexte pour parler des conteneurs de Python
from collections import OrderedDict

# pour exporter
import pickle as pl


# CONSTANTES - convention : en majuscule
DICO_NOM_MOIS = {1: 'janv', 2: 'févr', 3: 'mars', 4: 'avr', 5: 'mai', 6: 'juin', 7: 'juil', 8: 'août',
                 9:'sept', 10:'oct', 11:'nov', 12:'déc'}

DICO_COULEURS_MOIS = {1: "dimgrey", 2: "black", 3: "palegreen", 4: 'mediumspringgreen', 5: 'forestgreen', 6: 'gold', 7: 'orange',
                      8:'orangered', 9:'deepskyblue', 10:'royalblue', 11:'navy', 12:'silver'}

# les noms de fichiers de données
REP_DONNEES = "C:/WorkSpace/2021-Tuto-Python/Code/donnees"  # remplacez par votre chemin
TD4_ENTPE_TPQ_original = "TD4-donneesTPQ.txt"         # date au format "janv-00" : bon exercice pour "parser" en utilisant un dico
TD4_ENTPE_pv = "ChroniquesTPluieQ_pointvirgule.csv"   # même données avec des dates au format JJ/MM/AAAA, séparateur = ;
TD4_ENTPE_tab = "ChroniquesTPQ_tabulations.txt"       # même données avec des dates au format JJ/MM/AAAA, séparateur = blanc ou tabulation

HYDRO_QJO = "B2220010_qj_hydro2_2019.txt"
HYDRO_Qt = "B2220010_qtvar.txt"
HYDROQJ = "Y2372010_qj.csv"


trace_plt = True
trace_pd = True

# CLASSES  (même si en fait l'ordre importe peu...)

# FONCTIONS

def afficher_legende_code_couleur_mois(titre="Code couleur \n des mois"):
    # on crée
    plt.ion()
    fig, ax = plt.subplots()
    ax.axis('off')
    handles = [Line2D([0], [0], marker='o', color="b", label=DICO_NOM_MOIS[mois],
                      markerfacecolor=DICO_COULEURS_MOIS[mois], markersize=15) for mois in range(1, 13)]
    fig.legend(handles=handles, ncol=1, title=titre, fontsize=8, title_fontsize=10,
               labelspacing=1.5)
    # plt.show(block = False)
    """
    if Path.is_file(Path(IKONKA)):
        thismanager = plt.get_current_fig_manager()
        thismanager.window.tk.call('wm', 'iconphoto', thismanager.window._w, Photoimage(file=IKONKA))
    """
    fig.canvas.draw()
    plt.ioff()

# GRAPHIQUE SIMPLE

# format Hydro2  "QJO"

def lecteur_qj0_to_ts(nom_fichier, trace=True):
    """
    Lecture d'un fichier chronique QJ0 pour la création d'un objet de type Pandas.Series
    utilise le context manager with open(nom_fichier, 'r') as fichier, l'instructions readline et une boucle sur les lignes
    :param nom_fichier: chemin complet vers le fichier au format Hydro2 "QJ0"
    :param trace: booléen, si oui on trace un graphique simple
    :return: Pandas.Series
    """
    dico_qj = dict()
    code_station = None

    with open(nom_fichier, 'r') as fichier:
        while code_station is None:
            # lecture d'une ligne à la fois
            ligne = fichier.readline()
            if ligne[0:3] == "950":                   # du premier caractère (rang 0) au 3e (rang 3 exclu)
                vecteur = ligne.strip().split(";")
                code_station = vecteur[1].strip()
                nom_station = vecteur[2].strip()
        # normalement, on a trové 950 et on continue
        # un fichier est ITERABLE : boucle de lecture de toutes les lignes restantes du fichier
        for ligne in fichier:
            if "QJO" in ligne and code_station is not None:
                # on a vérifié que la ligne contient QJO ; on s'attend à QJO;B2220010;19680627;7330.000;C;9;
                # on découpe selon les ";" en une liste de chaîne de caracteres 
                liste_de_strings = ligne.strip().split(";")
                #  on vérifie que le 2e item correspond au code station attendu
                # et on va extraire une info AAAAMMJJ du 3e item puis lire la valeur numérique du 4e
                if liste_de_strings[1].strip() == code_station:
                    date_amj = liste_de_strings[2].strip()
                    annee = date_amj[0:4]
                    mois = date_amj[4:6]
                    jour = date_amj[6:8]
                    debit_lu = liste_de_strings[3].strip()
                    # tous ces éléments sont des STRING = CHAINES DE CARACTERES ; on a besoin de les convertir en nombres : int ou float
                    date = datetime(int(annee), int(mois), int(jour))   # objet de type datetime
                    # j'ajoute un élément au dictionnaire, de CLE  date (objet date) et de VALEUR  débit_lu converti en m3/S
                    dico_qj[date] = float(debit_lu) / 1000


    # on regarde ce que ça donne, au plus simple
    # à savoir : les méthodes de dictionnaire keys() et values() ne renvoient pas une liste mais une vue
    # donc on doit convertir en liste ; matplotlib ne sait pas gérer les dictionnaires tels quels
    plt.plot(list(dico_qj.keys()), list(dico_qj.values()))
    plt.title(nom_station)
    plt.ylabel("QJ, débit moyen journalier (m3/s)")
    plt.show()
    # le programme s'arrête ici jusqu'à ce que l'on ferme la figure
    
    # je vais maintenant convertir mon dictionnaire en pandas.Series, type qui offre des fonctionnalités intéressantes

    print(f"Lecture terminée, de {min(dico_qj.keys())} à {max(dico_qj.keys())} ")
    print(f" {len(dico_qj)} valeurs lues, de  {min(dico_qj.items())} à {max(dico_qj.items())} ")
    print(f" Code station =  {code_station} , nom = {nom_station}")

    qj_series = pd.Series(dico_qj, name=nom_station + ' (QJO)').resample("1D").asfreq()
    qj_series.sort_index(inplace=True)

    # on va utiliser le wrapper de pandas pour tracer :   objet_pandas.plot
    if trace:
        print( "on va utiliser le wrapper de pandas pour tracer" )
        fig, ax = plt.subplots(figsize=(10, 5))

        qj_series.plot(drawstyle="steps-post")
        ax.set_ylabel("QJ, débit moyen journalier (m3/s)")

        fig.legend()
        ax.set_title("Avec pandas QHydro2 QJ0, station " + nom_station )

        plt.show()

    return qj_series


def donnees_TD_ETP_csv_panda_seul(nom_fichier, separateur=";"):

    """
    Lecteur du fichier TD4  ChroniquesTPluieQ_pointvirgule
     caractéristiques : les dates sont bien reconnaissables, le séparateur de champs est le ;
     on a donc définir un argument "facultatif" avec une valeur par défaut, si on ne précise rien le séparateur sera bien le ";"
     version "compacte" mais il serait sans doute possible de lire direcement les dates avec les arguments supplémentaires
     ..., parse_dates=True, dayfirst=True, infer_datetime_format=True,index_col=[0] )
     mais ça n'a pas marché... alors on passera par une étape intermédiaire

    """
    #note :     # https: // stackoverflow.com / questions / 25416955 / plot - pandas - dates - in -matplotlib

    DF_TD4 = pd.read_csv(nom_fichier, sep=separateur, decimal=',')

    #on affiche les 5 premières lignes pour vérifier
    DF_TD4.head(5)

    nom_colonnes = DF_TD4.columns.tolist()
    print("Noms de colonnes lus dans le csv : ", nom_colonnes)

    # on affiche les 15 premières valeurs de la colonne n°0
    print(DF_TD4[nom_colonnes[0]][:15])

    # pour assigner un format date à la première colonne,
    # on s'en remet à pandas, qui "se débrouille pour lire une date si c'est une date"
    # date_au_format_pandas = pd.to_datetime(un_truc_nimporte_comment_qui_est_une_date)
    # piège ; on  ne sait jamais si c'est MMDD ou DDMM : l'argument dayfirst=True permet de cadrer le déchiffrage

    verif_conversion_dates = True
    if verif_conversion_dates :
        liste_dates_td = pd.to_datetime(DF_TD4[nom_colonnes[0]], dayfirst=True)   # dayfirst est important !
        print("vérif conversion dates : ", liste_dates_td[:15])
        # DF_TD4[nom_colonnes[0]]= pd.to_datetime(DF_TD4[nom_colonnes[0]], dayfirst=True)

    # une fois convaincus, on peut faire la conversion directement dans la colonne
    DF_TD4[nom_colonnes[0]] = pd.to_datetime(DF_TD4[nom_colonnes[0]], dayfirst=True)

    # on déclare comme index la colonne, avec les dates ; elle prend donc un statut particulier
    DF_TD4.set_index(nom_colonnes[0], inplace=True)

    # on met à jour la liste des colonnes (on a supprimé une colonne "dates", qui est devenue l'index)
    nom_colonnes = DF_TD4.columns.tolist()
    print("Noms des colonnes après avoir défini l'index : ", nom_colonnes)

    print("Type de liste dates td", type(liste_dates_td[0]), liste_dates_td[0])
    print("Type de la première colonne (0) ", type(DF_TD4[nom_colonnes[0]][0]), DF_TD4[nom_colonnes[0]][0])

    DF_TD4.head(5)

    # idem, on laisse pandas se débrouiller complètement
    DF_TD4.plot()
    plt.suptitle("En utilisant les possibilités de pandas")
    plt.legend()
    plt.show()

    return DF_TD4


def donnees_TD_ETP_2subplots(nom_fichier):
    # le début est identique à donnees_TD_ETP_csv

    DF_TD4 = pd.read_csv(nom_fichier, sep=';', decimal=',')
    nom_colonnes = DF_TD4.columns.tolist()
    DF_TD4[nom_colonnes[0]] = pd.to_datetime(DF_TD4[nom_colonnes[0]], dayfirst=True)

    # on définit index=première colonne, la colonne "date" ,'existe donc plus en temps que telle
    DF_TD4.set_index(nom_colonnes[0], inplace=True)

    liste_dates = [date_pd.to_pydatetime() for date_pd in DF_TD4.index]

    DF_TD4.head(5)
    nom_colonnes = DF_TD4.columns.tolist()
    print("Noms de colonnes lus dans le csv : ", nom_colonnes)


    #première figure : PANDAS
    # méthode plot de pandas
    DF_TD4.plot()
    plt.suptitle("En utilisant les possibilités de pandas")
    plt.gcf().canvas.set_window_title("PTQ_avec_pandas")
    plt.legend()

    # plt.gca().format_xdata = mpldates.DateFormatter('%Y/%m')
    # plt.gca().format_ydata = un_chiffre_apres_la_virgule
    # plt.gcf().autofmt_xdate()   # sur figure
    plt.show()
    plt.close()

    # on découpe la figure en deux subplots
    fig, (ax_pluie, ax_q) = plt.subplots(ncols=1, nrows=2, sharex=True)
    fig.canvas.set_window_title("PTQ_2subplots")
    fig.subplots_adjust(bottom=0.15)
    fig.suptitle("Forçages en haut, réponse du bassin en bas")

    etiquette_pluie = nom_colonnes[1]
    ax_pluie.set_ylabel(etiquette_pluie, color='lightblue')
    ax_pluie.vlines(x=liste_dates, ymin=0, ymax=DF_TD4[etiquette_pluie].tolist(), color='lightblue', lw=2,
                    label=etiquette_pluie)
    ax_pluie.invert_yaxis()

    ax_t = ax_pluie.twinx()
    etiquette_temperatures = nom_colonnes[0]
    ax_t.set_ylabel(etiquette_temperatures)
    ax_t.plot(liste_dates, DF_TD4[etiquette_temperatures].tolist(), marker='*', color='orange', ls=':',
              label=etiquette_temperatures)

    etiquette_debit = nom_colonnes[2]
    ax_q.set_ylabel(etiquette_debit, color='blue')
    ax_q.plot(liste_dates, DF_TD4[etiquette_debit].tolist(), marker='>', color='blue', ls=':', label=etiquette_debit)

    fig.legend(bbox_to_anchor=(1, 0), loc="lower right",
               bbox_transform=fig.transFigure, ncol=4)

    plt.show()

    return DF_TD4

def donnees_TD_ETP_2subplots_variante(DF_PTQ):
    """
    # on prend en argument un DataFrame
    et on fait un graphique avec d'autres solutions que plot et lines
    """
    # couleurs par mois : prétexte pour manipuler dictionnaires et scatter
    # équivalent "plus lisible" de :  liste_couleurs = [ DICO_COULEURS_MOIS[date.month] for date in DF_PTQ.index]
    liste_couleurs = []
    for date in DF_PTQ.index:
        liste_couleurs.append(DICO_COULEURS_MOIS[date.month])

    nom_colonnes = DF_PTQ.columns.tolist()
    # on découpe la figure en deux subplots
    fig, (ax_pluie, ax_q) = plt.subplots(ncols=1, nrows=2, sharex=True)
    fig.canvas.set_window_title("PTQ_2subplots")
    fig.subplots_adjust(bottom=0.15)
    fig.suptitle("Forçages en haut, réponse du bassin en bas")

    etiquette_pluie = nom_colonnes[1]
    ax_pluie.set_ylabel(etiquette_pluie, color='lightblue')
    ax_pluie.step(x=DF_PTQ.index, y=DF_PTQ[etiquette_pluie].tolist(), color='lightblue', lw=2,
                    label=etiquette_pluie, where='post')
    ax_pluie.fill_between(DF_PTQ.index, DF_PTQ[etiquette_pluie].tolist(), step="post", alpha=0.4)
    ax_pluie.invert_yaxis()

    ax_t = ax_pluie.twinx()
    etiquette_temperatures = nom_colonnes[0]
    ax_t.set_ylabel(etiquette_temperatures)
    ax_t.step(DF_PTQ.index, DF_PTQ[etiquette_temperatures].tolist(), marker='None', color='orange', ls=':',
              label=etiquette_temperatures + " (step)", where="post")
    # todo : on  a un peu forcé le "scatter" pour s'exercer
    ax_t.scatter(DF_PTQ.index, DF_PTQ[etiquette_temperatures].tolist(), marker='*', ls=':',
                 label=etiquette_temperatures, c=liste_couleurs)

    etiquette_debit = nom_colonnes[2]
    ax_q.set_ylabel(etiquette_debit, color='blue')
    ax_q.step(DF_PTQ.index, DF_PTQ[etiquette_debit].tolist(), marker='None', color='blue', ls='-', label=etiquette_debit, where='post')

    fig.legend(bbox_to_anchor=(1, 0), loc="lower right",
               bbox_transform=fig.transFigure, ncol=4)

    #le module pickle
    # nom_fichier_pickle = Path(nom_fichier).with_suffix('.pickle')
    # pl.dump(fig, open(nom_fichier_pickle, 'wb'))
    # print(nom_fichier_pickle)

    plt.show()
    # pas d'instruction return, mais par défaut la fonction retourne quand même...   None

def donnees_TD_ETP_2subplots_variante2(DF_PTQ):
    """
    # on prend en argument un DataFrame
    et on fait un graphique avec d'autres solutions que plot et lines
    """
    # couleurs par mois : prétexte pour manipuler dictionnaires et scatter
    # équivalent "plus lisible" de :  liste_couleurs = [ DICO_COULEURS_MOIS[date.month] for date in DF_PTQ.index]
    liste_couleurs = []
    for date in DF_PTQ.index:
        liste_couleurs.append(DICO_COULEURS_MOIS[date.month])


    nom_colonnes = DF_PTQ.columns.tolist()
    # on découpe la figure en deux subplots
    fig, (ax_pluie, ax_codemois, ax_q) = plt.subplots(ncols=1, nrows=3, sharex=True)
    fig.canvas.set_window_title("PTQ_2subplots")
    fig.subplots_adjust(bottom=0.15)
    fig.suptitle("Forçages en haut, réponse du bassin en bas")

    etiquette_pluie = nom_colonnes[1]
    ax_pluie.set_ylabel(etiquette_pluie, color='lightblue')
    ax_pluie.step(x=DF_PTQ.index, y=DF_PTQ[etiquette_pluie].tolist(), color='lightblue', lw=2,
                    label=etiquette_pluie, where='post')
    ax_pluie.fill_between(DF_PTQ.index, DF_PTQ[etiquette_pluie].tolist(), step="post", alpha=0.4)
    ax_pluie.invert_yaxis()

    ax_t = ax_pluie.twinx()
    etiquette_temperatures = nom_colonnes[0]
    ax_t.set_ylabel(etiquette_temperatures)
    ax_t.step(DF_PTQ.index, DF_PTQ[etiquette_temperatures].tolist(), marker='None', color='orange', ls=':',
              label=etiquette_temperatures + " (step)", where="post")
    # todo : on  a un peu forcé le "scatter" pour s'exercer
    ax_t.scatter(DF_PTQ.index, DF_PTQ[etiquette_temperatures].tolist(), marker='*', ls=':',
                 label=etiquette_temperatures, c=liste_couleurs)

    etiquette_debit = nom_colonnes[2]
    ax_q.set_ylabel(etiquette_debit, color='blue')
    ax_q.step(DF_PTQ.index, DF_PTQ[etiquette_debit].tolist(), marker='None', color='blue', ls='-', label=etiquette_debit, where='post')

    fig.legend(bbox_to_anchor=(1, 0), loc="lower right",
               bbox_transform=fig.transFigure, ncol=4, title="Valeurs moyennes mensuelles")

    # on enlève ax_codemois de la liaison
    #  https://stackoverflow.com/questions/42973223/how-to-share-x-axes-of-two-subplots-after-they-have-been-created
    ax_codemois.autoscale()
    ax_codemois.axis('off')
    handles = [Line2D([0], [0], marker='*', ls="None", label=DICO_NOM_MOIS[mois],markerfacecolor=DICO_COULEURS_MOIS[mois],
                      markeredgecolor=DICO_COULEURS_MOIS[mois], markersize=7) for mois in range(1, 13)]
    ax_codemois.legend(handles=handles, ncol=6, title="Code couleur des mois", fontsize=9, title_fontsize=9,
               labelspacing=1.5)

    #le module pickle
    # nom_fichier_pickle = Path(nom_fichier).with_suffix('.pickle')
    # pl.dump(fig, open(nom_fichier_pickle, 'wb'))
    # print(nom_fichier_pickle)

    plt.show()
    # pas d'instruction return, mais par défaut la fonction retourne quand même...   None

#  LIRE UN FICHIER BINAIRE
def lire_lame_deau_radar_5min(nom_fichier_binaire):
    PDT_OBS_EN_MIN = 5
    DIVISEUR = 10
    CODE_LACUNE_BINAIRE = 65535  # => -999.99 (lacune)
    CODE_LACUNE = -999.99

    nb_of_days = 3653
    pas_de_temps = 24 * 60 / PDT_OBS_EN_MIN  # nb de pas de temps dans une journée ; = 288
    # nb_valeurs_cp = nb_of_days * pas_de_temps = 1514972160
    len_of_tvect = nb_of_days * pas_de_temps  # 1052064
    recordSize = len_of_tvect * 2  # parce que ce sont des entiers codés sur 2 bytes

    date_premiere_valeur = datetime(2006, 7, 1, 0, 5)
    liste_dates = [date_premiere_valeur + i * timedelta(minutes=PDT_OBS_EN_MIN) for i in
                   range(len_of_tvect)]  # PDT_OBS_EN_MIN =  5 minutes

    if Path.is_file(Path(nom_fichier_binaire)):
        print(f"nom_chemin_binaire1 : {nom_fichier_binaire} ")

        # lecture des valeurs
        # cf fonctions Chronique.lire_chronique Lecture_Patnthere...chronique
        uint16_valeurs = None
        with open(nom_fichier_binaire, 'rb') as fic_binaire:
            try:
                uint16_valeurs = np.fromfile(fic_binaire, dtype=np.uint16, count=int(len(liste_dates)))
                statut_lecture = "OK"
            except:
                statut_lecture = "erreur"

        # VERIF
        print("Ensemble des valeurs lues avant traitement (lacunes, diviseur...) : ", set(uint16_valeurs))

        if statut_lecture == "OK":
            # Traitement des lacunes
            nb_lacunes = 0
            for valeur in uint16_valeurs:
                if int(valeur) == int(CODE_LACUNE_BINAIRE):
                    nb_lacunes += 1
            print("Nombre de lacunes = ", nb_lacunes)

            # on travaille avec des LAMES D'EAU en centième de mm, à passer en mm
            np_valeurs = np.array(
                [entier / DIVISEUR if entier != int(CODE_LACUNE_BINAIRE) else np.nan for entier in
                 uint16_valeurs])

            # tracé
            debut = datetime.now()
            plt.step(liste_dates, np_valeurs)
            plt.title("Fichier binaire "+ os.path.basename(nom_fichier_binaire) + "\n sans précaution pour les étiquettes de dates" )
            temps = datetime.now() - debut
            plt.show()

            plt.close()
            print("temps écoulé, step:", temps)

            return  liste_dates, np_valeurs

#  Tracer une longue série, applicable à toutes donnnées (lues dans Hydro2, dans un binaire ou tout autre format)
def tracer_proprement_une_longue_serie(dates, valeurs, info_chroniques="lames d'eau radar, mm en 5 min"):
    """ démonstration de "concise formatter"

    """
    locator = mdates.AutoDateLocator(minticks=3, maxticks=12)
    formatter = mdates.ConciseDateFormatter(locator)

    # VERIF
    if len(dates) != len(valeurs):
        print(f"Les 2 vecteurs  n'ont pas la même longueur : {len(dates)} dates et {len(valeurs)} valeurs")
        return
        # ça permet de quitter la fonction ; ce n'est pas la meilleure manière de faire (plutôt un else...)

    print("Nombre de points : ", len(dates))

    # tracé
    debut = datetime.now()
    plt.ion()
    fig_cf, ax_t = plt.subplots()
    fig_cf.suptitle("Tracé d'une longue chronique, soin apporté aux étiquettes de dates \n" + info_chroniques)
    ax_t.set_xlabel("dates, format date", fontsize=14)
    ax_t.set_ylabel("info_valeurs", fontsize=14)
    ax_t.xaxis.set_major_locator(locator)
    ax_t.xaxis.set_major_formatter(formatter)
    ax_t.step(dates, valeurs)

    temps = datetime.now() - debut
    print("tracer_proprement_une_longue_serie, temps écoulé, step:", temps)
    fig_cf.canvas.draw()
    fig_cf.savefig("test.png")
    plt.ioff()
    plt.show()

    #plt.close()

    # on ne retourne rien


def tracer_une_longue_serie_en3_resolutions_tests(dates, valeurs, info_chroniques=" série temporelle", info_valeurs="lames d'eau radar, mm en 5 min"):
    # met en lumière des problèmes liés à pandas.resample
    # pour les données binaires, voir en particulier le pic max, situé un 30 juin à 17h30, avec une max mensuel rebasculé au mois suivant

    locator = mdates.AutoDateLocator(minticks=3, maxticks=12)
    formatter = mdates.ConciseDateFormatter(locator)

    couleur_originale = 'purple'
    dico_resolutions = {"jour":"1D", "mois":"1M", "année":"1Y"}
    dico_resolutions_couleurs = {"jour": "blue", "mois": "green", "année": "orange"}

    fig, (ax_original, ax_resample_max) = plt.subplots(nrows=2, ncols=1, sharex=True)
    fig.subplots_adjust(bottom=0.3)

    fig.suptitle(info_valeurs +  "\n Rééchantillonnages sur pdt fixes, en utilisant pandas")
    ax_resample_moy = ax_original.twinx()
    ax_original.set_ylabel("Données originales", color=couleur_originale)
    ax_resample_moy.set_ylabel("Moyennes sur pdt fixes", color='black')
    ax_resample_max.set_ylabel("Max sur pdt fixes", color='black')
    # méthode plot de pandas appliquée à un dataframe

    ax_original.step(dates, valeurs, c=couleur_originale, label="résolution du fichier")
    #ax_original.plot(dates, valeurs, c=couleur_originale, label="résolution du fichier", marker='*', markersize=10, ls="None")
    ax_original.legend()

    ts= pd.Series(data=valeurs, index=dates, name=info_valeurs)
    # test d'un axe supplémentaire (3e) avec twinx : il va être superposé à l'axe déjà défini par twinx
    if False:
        ax_test = ax_original.twinx()
        ax_test.set_ylabel("test", color="sienna")
        ts_test = ts.resample('1D', closed='left').mean()
        ax_test.plot(ts_test.index.to_pydatetime(), ts_test.to_numpy(), c='sienna',
                         label="test", marker='None', ls="None")
    # on définit une liste ax_moy dans le cas où l'on veut tester un "ax" par résolution : résultat, les axes des y sont superposés
    #ax_moy=[]

    for duree, reso in dico_resolutions.items():
        ts_moy = ts.copy().resample(reso, closed='left',label='left').mean()
        #ax_moy.append(ax_original.twinx())
        print(f"Rééchantillonnage par moyenne sur pas de temps {duree}, première date = {ts_moy.index[0]}")
        # ax_resample_moy.step(ts_moy.index, ts_moy.to_numpy(), c=dico_resolutions_couleurs[duree], label=duree)
        ax_resample_moy.plot(ts_moy.index.to_pydatetime(), ts_moy.to_numpy(), c=dico_resolutions_couleurs[duree],
                        label=duree, marker='*', ls="None")

        if reso=="1D":
            ax_resample_moy.step(ts_moy.index.to_pydatetime(), ts_moy.to_numpy(), c=dico_resolutions_couleurs[duree],
                        label=duree, where='post')
        else:
            ax_resample_moy.step(ts_moy.index.to_pydatetime(), ts_moy.to_numpy(), c=dico_resolutions_couleurs[duree],
                                 label=duree)
        """
        ax_moy[-1].plot(ts_moy.index.to_pydatetime(), ts_moy.to_numpy(), c=dico_resolutions_couleurs[duree],
                             label=duree, marker='*', ls="None")
        ax_moy[-1].step(ts_moy.index.to_pydatetime(), ts_moy.to_numpy(), c=dico_resolutions_couleurs[duree], label=duree) # , where='post')
        """
        ts_max = ts.resample(reso, closed='left', label='left').max()
        print(f"Rééchantillonnage par max sur pas de temps {duree}, première date = {ts_max.index[0]}")
        # ax_resample_max.step(ts_max.index, ts_max.to_numpy(), c=dico_resolutions_couleurs[duree], label=duree)
        ax_resample_max.plot(ts_max.index.to_pydatetime(), ts_max.to_numpy(), c=dico_resolutions_couleurs[duree],
                             label=duree, marker='*', ls="None")
        if reso=="1D":
            ax_resample_max.step(ts_max.index.to_pydatetime(), ts_max.to_numpy(), c=dico_resolutions_couleurs[duree], label=duree, where='post')
        else:
            ax_resample_max.step(ts_max.index.to_pydatetime(), ts_max.to_numpy(), c=dico_resolutions_couleurs[duree],
                                 label=duree)


    ax_resample_max.legend()

    ax_original.xaxis.set_major_locator(locator)
    ax_original.xaxis.set_major_formatter(formatter)

    plt.show()

def tracer_une_longue_serie_en3_resolutions_mieux(dates, valeurs, info_chroniques="lames d'eau radar, mm en 5 min"):
    """
    solutions proposées au pb identifié
    """

    # format the coords message box
    def un_chiffre_apres_la_virgule(x):
        # return '%1.1f unités' % x
        return f"{x:.1f} mm/5 min"

    # imports spécifiques
    from mpl_toolkits.axes_grid1 import host_subplot
    import mpl_toolkits.axisartist as AA

    locator = mdates.AutoDateLocator(minticks=3, maxticks=12)
    formatter = mdates.ConciseDateFormatter(locator)

    fig = plt.figure(dpi=100)  # , frameon=False figsize=(20, 4),
    fig.patch.set_facecolor('white')
    fig.subplots_adjust(right=0.75, bottom=0.15)  # on définit des marges
    fig.suptitle(info_chroniques + "\n Rééchantillonnages sur pdt fixes, en utilisant pandas")

    # VIGNETTE DU HAUT
    host_original = host_subplot(211, axes_class=AA.Axes)
    par_j = host_original.twinx()
    par_m = host_original.twinx()
    par_a = host_original.twinx()

    offset = 40
    new_fixed_axis_j = par_j.get_grid_helper().new_fixed_axis
    par_j.axis["right"] = new_fixed_axis_j(loc="right", axes=par_j, offset=(0, 0))
    par_j.axis["right"].toggle(all=True)
    new_fixed_axis_m = par_m.get_grid_helper().new_fixed_axis
    par_m.axis["right"] = new_fixed_axis_m(loc="right", axes=par_m, offset=(offset, 0))
    par_m.axis["right"].toggle(all=True)
    new_fixed_axis_a = par_a.get_grid_helper().new_fixed_axis
    par_a.axis["right"] = new_fixed_axis_a(loc="right", axes=par_a, offset=(2*offset, 0))
    par_a.axis["right"].toggle(all=True)

    # vignette du bas
    ax_unique = fig.add_subplot(212, sharex =host_original)

    # tracé donnée à la résolution originale,
    ts = pd.Series(data=valeurs, index=dates, name=info_chroniques)
    couleur_originale = 'purple'
    host_original.set_ylabel("Données originales", color=couleur_originale)
    host_original.step(dates, valeurs, c=couleur_originale)
    ax_unique.set_ylabel("Max sur pas de temps fixe", color='black')
    ax_unique.step(dates, valeurs, c=couleur_originale, label="résolution du fichier")

    dico_resolutions = {"jour":"1D", "mois":"1M", "année":"1Y"}
    dico_resolutions_couleurs = {"jour": "blue", "mois": "green", "année": "orange"}

    # vignette du haut, moyennes

    par_j.set_ylabel("Moyennes journalières", color=dico_resolutions_couleurs["jour"])
    par_m.set_ylabel("Moyennes mensuelles", color=dico_resolutions_couleurs["mois"])
    par_a.set_ylabel("Moyennes annuelles", color=dico_resolutions_couleurs["année"])

    ts_j_moy = ts.resample("1D").mean()
    print(f"Rééchantillonnage par moyenne sur pas de temps {'jour'}, première date = {ts_j_moy.index[0]}")
    par_j.plot(ts_j_moy.index.to_pydatetime(), ts_j_moy.to_numpy(), c=dico_resolutions_couleurs["jour"],
                        marker='*', ls="None")
    par_j.step(ts_j_moy.index.to_pydatetime(), ts_j_moy.to_numpy(), c=dico_resolutions_couleurs["jour"],
                        label="jour", where='post')

    ts_m_moy = ts_j_moy.resample("1M").mean()
    print(f"Rééchantillonnage par moyenne sur pas de temps {'mois'}, première date = {ts_m_moy.index[0]}")
    par_m.plot(ts_m_moy.index.shift(1, freq='D').to_pydatetime(), ts_m_moy.to_numpy(), c=dico_resolutions_couleurs["mois"],
                        marker='*', ls="None")
    par_m.step(ts_m_moy.index.shift(1, freq='D').to_pydatetime(), ts_m_moy.to_numpy(), c=dico_resolutions_couleurs["mois"],
                        label="mois")

    ts_a_moy = ts_m_moy.resample("1Y").mean()
    print(f"Rééchantillonnage par moyenne sur pas de temps {'année'}, première date = {ts_a_moy.index[0]}")
    par_a.plot(ts_a_moy.index.shift(1, freq='D').to_pydatetime(), ts_a_moy.to_numpy(), c=dico_resolutions_couleurs["année"],
                        marker='*', ls="None")
    par_a.step(ts_a_moy.index.shift(1, freq='D').to_pydatetime(), ts_a_moy.to_numpy(), c=dico_resolutions_couleurs["année"],
                        label="année")

    #  vignette du bas, max
    ts_j_max = ts.resample("1D").max()
    print(f"Rééchantillonnage par max sur pas de temps {'jour'}, première date = {ts_j_moy.index[0]}")
    # ax_resample_moy.step(ts_moy.index, ts_moy.to_numpy(), c=dico_resolutions_couleurs[duree], label=duree)
    ax_unique.plot(ts_j_max.index.to_pydatetime(), ts_j_max.to_numpy(), c=dico_resolutions_couleurs["jour"],
               marker='*', ls="None")
    ax_unique.step(ts_j_max.index.to_pydatetime(), ts_j_max.to_numpy(), c=dico_resolutions_couleurs["jour"],
               where='post')

    ts_m_max = ts_j_max.resample("1M").max()
    print(f"Rééchantillonnage par max sur pas de temps {'mois'}, première date = {ts_m_moy.index[0]}")
    # ax_resample_moy.step(ts_moy.index, ts_moy.to_numpy(), c=dico_resolutions_couleurs[duree], label=duree)
    ax_unique.plot(ts_m_max.index.shift(1, freq='D').to_pydatetime(), ts_m_max.to_numpy(), c=dico_resolutions_couleurs["mois"],
               marker='*', ls="None")
    ax_unique.step(ts_m_max.index.shift(1, freq='D').to_pydatetime(), ts_m_max.to_numpy(), c=dico_resolutions_couleurs["mois"])
    ts_a_max = ts_m_max.resample("1Y").max()
    print(f"Rééchantillonnage par max sur pas de temps {'année'}, première date = {ts_a_moy.index[0]}")
    ax_unique.plot(ts_a_max.index.shift(1, freq='D').to_pydatetime(), ts_a_max.to_numpy(), c=dico_resolutions_couleurs["année"],
                       marker='*', ls="None")
    ax_unique.step(ts_a_max.index.shift(1, freq='D').to_pydatetime(), ts_a_max.to_numpy(), c=dico_resolutions_couleurs["année"])

    host_original.xaxis.set_major_locator(locator)
    host_original.xaxis.set_major_formatter(formatter)

    fig.legend(loc='lower center', ncol=4)

    host_original.format_ydata = un_chiffre_apres_la_virgule

    plt.show()

def tracer_une_longue_serie_lame_deau_en3_resolutions_verifs(dates, valeurs, info_resolution_min=5, couverture_mini = 0.8, info_chroniques="lames d'eau radar, mm en 5 min"):
    """
    Cas de lames d'eau : on aggrège par CUMUL
    solutions proposées au pb identifié :
    + boucle "propre", possible car on ne distingue plus le cas journalier des autres
    + vérif nb points disponibles avec resample().count
    https://stackoverflow.com/questions/49019245/resample-pandas-with-minimum-required-number-of-observations
    todo : reste un défaut, les 0 ne sont pas forcément alignés sur la même horizontale !
    """


    # format the coords message box
    def un_chiffre_apres_la_virgule(x):
        # return '%1.1f unités' % x
        return f"{x:.1f} mm/5 min"

    # imports spécifiques
    from mpl_toolkits.axes_grid1 import host_subplot
    import mpl_toolkits.axisartist as AA

    locator = mdates.AutoDateLocator(minticks=3, maxticks=12)
    formatter = mdates.ConciseDateFormatter(locator)

    fig = plt.figure(dpi=100)  # , frameon=False figsize=(20, 4),
    fig.patch.set_facecolor('white')
    fig.subplots_adjust(right=0.75, bottom=0.15)  # on définit des marges
    fig.suptitle(info_chroniques + f"\n Rééchantillonnages sur pdt fixes, taux de couverture mini = {int(couverture_mini*100)}%")

    # VIGNETTE DU HAUT
    host_original = host_subplot(211, axes_class=AA.Axes)
    par_j = host_original.twinx()
    par_m = host_original.twinx()
    par_a = host_original.twinx()

    offset = 40
    new_fixed_axis_j = par_j.get_grid_helper().new_fixed_axis
    par_j.axis["right"] = new_fixed_axis_j(loc="right", axes=par_j, offset=(0, 0))
    par_j.axis["right"].toggle(all=True)
    new_fixed_axis_m = par_m.get_grid_helper().new_fixed_axis
    par_m.axis["right"] = new_fixed_axis_m(loc="right", axes=par_m, offset=(offset, 0))
    par_m.axis["right"].toggle(all=True)
    new_fixed_axis_a = par_a.get_grid_helper().new_fixed_axis
    par_a.axis["right"] = new_fixed_axis_a(loc="right", axes=par_a, offset=(2*offset, 0))
    par_a.axis["right"].toggle(all=True)

    # vignette du bas
    ax_unique = fig.add_subplot(212, sharex =host_original)

    # tracé donnée à la résolution originale,
    ts = pd.Series(data=valeurs, index=dates, name=info_chroniques)
    couleur_originale = 'purple'
    host_original.set_ylabel("Données originales", color=couleur_originale)
    host_original.step(dates, valeurs, c=couleur_originale)
    ax_unique.set_ylabel("Max sur pas de temps fixe", color='black')
    ax_unique.step(dates, valeurs, c=couleur_originale, label="résolution du fichier")

    dico_resolutions = {"jour":"1D", "mois":"1M", "année":"1Y"}
    dico_resolutions_couleurs = {"jour": "blue", "mois": "green", "année": "orange"}

    # vignette du haut, moyennes

    par_j.set_ylabel("Cumuls journaliers", color=dico_resolutions_couleurs["jour"])
    par_m.set_ylabel("Cumuls mensuels", color=dico_resolutions_couleurs["mois"])
    par_a.set_ylabel("Cumuls annuels", color=dico_resolutions_couleurs["année"])

    nb_minutes_attendues_par_jour =  couverture_mini * 24*60 /  info_resolution_min

    for ((reso, code_reso), axe_parasite) in zip(dico_resolutions.items(), [par_j, par_m, par_a]):

        #  vignette du haut, moyennes
        df_reso_moy = ts.resample(code_reso).agg(['sum', 'count']) # une colonne moyenne, une colonne  nombre de points dans la fenêtre

        # définition d'un "masque" de booléens par une condition
        if reso=="jour":
            invalid = df_reso_moy['count'] <=  nb_minutes_attendues_par_jour
        elif reso=="mois":
            invalid = df_reso_moy['count'] <= df_reso_moy.index.days_in_month * nb_minutes_attendues_par_jour
        else:
            invalid = df_reso_moy['count'] <= 365 * nb_minutes_attendues_par_jour

        # restrict to the sum and null out invalid entries, using invalid as a mask
        # https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html#returning-a-view-versus-a-copy
        df_reso_moy.loc[invalid, 'sum'] = np.nan
        ts_reso_moy = df_reso_moy['sum']
        print(f"Rééchantillonnage par moyenne sur pas de temps {reso}, première date = {ts_reso_moy.index[0]}")
        axe_parasite.plot(ts_reso_moy.index.shift(1, freq='D').to_pydatetime(), ts_reso_moy.to_numpy(), c=dico_resolutions_couleurs[reso],
                        marker='*', ls="None")
        axe_parasite.step(df_reso_moy.index.shift(1, freq='D').to_pydatetime(), ts_reso_moy.to_numpy(), c=dico_resolutions_couleurs[reso],
                        label=reso)  # , where='post')

        #  vignette du bas, max

        ts_reso_max = ts.resample(code_reso).max()
        ts_reso_max[invalid] = np.nan
        print(f"Rééchantillonnage par max sur pas de temps {'jour'}, première date = {ts_reso_max[0]}")
        ax_unique.plot(ts_reso_max.index.shift(1, freq='D').to_pydatetime(), ts_reso_max.to_numpy(), c=dico_resolutions_couleurs[reso],
               marker='*', ls="None")
        ax_unique.step(ts_reso_max.index.shift(1, freq='D').to_pydatetime(), ts_reso_max.to_numpy(), c=dico_resolutions_couleurs[reso])


    host_original.xaxis.set_major_locator(locator)
    host_original.xaxis.set_major_formatter(formatter)

    fig.legend(loc='lower center', ncol=4)

    host_original.format_ydata = un_chiffre_apres_la_virgule

    plt.show()

def tracer_une_longue_serie_q_en3_resolutions_verifs(dates, valeurs, info_resolution_min=60, couverture_mini = 0.8, info_chroniques="débit moyen sur une journée"):
    """
    Cas de débits : on aggrège par MOYENNE
    solutions proposées au pb identifié :
    + boucle "propre", possible car on ne distingue plus le cas journalier des autres
    + vérif nb points disponibles avec resample().count

    https://stackoverflow.com/questions/49019245/resample-pandas-with-minimum-required-number-of-observations
    todo : reste un défaut, les 0 ne sont pas forcément alignés sur la même horizontale !
    """


    # format the coords message box
    def un_chiffre_apres_la_virgule(x):
        # return '%1.1f unités' % x
        return f"{x:.1f} mm/5 min"

    # imports spécifiques
    from mpl_toolkits.axes_grid1 import host_subplot
    import mpl_toolkits.axisartist as AA

    locator = mdates.AutoDateLocator(minticks=3, maxticks=12)
    formatter = mdates.ConciseDateFormatter(locator)

    fig = plt.figure(dpi=100)  # , frameon=False figsize=(20, 4),
    fig.patch.set_facecolor('white')
    fig.subplots_adjust(right=0.75, bottom=0.15)  # on définit des marges
    fig.suptitle(info_chroniques + f"\n Rééchantillonnages sur pdt fixes, taux de couverture mini = {int(couverture_mini*100)}%")

    # VIGNETTE DU HAUT
    host_original = host_subplot(211, axes_class=AA.Axes)
    par_j = host_original.twinx()
    par_m = host_original.twinx()
    par_a = host_original.twinx()

    offset = 40
    new_fixed_axis_j = par_j.get_grid_helper().new_fixed_axis
    par_j.axis["right"] = new_fixed_axis_j(loc="right", axes=par_j, offset=(0, 0))
    par_j.axis["right"].toggle(all=True)
    new_fixed_axis_m = par_m.get_grid_helper().new_fixed_axis
    par_m.axis["right"] = new_fixed_axis_m(loc="right", axes=par_m, offset=(offset, 0))
    par_m.axis["right"].toggle(all=True)
    new_fixed_axis_a = par_a.get_grid_helper().new_fixed_axis
    par_a.axis["right"] = new_fixed_axis_a(loc="right", axes=par_a, offset=(2*offset, 0))
    par_a.axis["right"].toggle(all=True)

    # vignette du bas
    ax_unique = fig.add_subplot(212, sharex =host_original)

    # tracé donnée à la résolution originale,
    ts = pd.Series(data=valeurs, index=dates, name=info_chroniques)
    couleur_originale = 'purple'
    host_original.set_ylabel("Données originales", color=couleur_originale)
    host_original.step(dates, valeurs, c=couleur_originale)
    ax_unique.set_ylabel("Max sur pas de temps fixe", color='black')
    ax_unique.step(dates, valeurs, c=couleur_originale, label="résolution du fichier")

    dico_resolutions = {"jour":"1D", "mois":"1M", "année":"1Y"}
    dico_resolutions_couleurs = {"jour": "blue", "mois": "green", "année": "orange"}

    # vignette du haut, moyennes

    par_j.set_ylabel("Moyennes journalières", color=dico_resolutions_couleurs["jour"])
    par_m.set_ylabel("Moyennes mensuelles", color=dico_resolutions_couleurs["mois"])
    par_a.set_ylabel("Moyennes annuelles", color=dico_resolutions_couleurs["année"])

    nb_minutes_attendues_par_jour =  couverture_mini * 24*60 /  info_resolution_min

    for ((reso, code_reso), axe_parasite) in zip(dico_resolutions.items(), [par_j, par_m, par_a]):

        #  vignette du haut, moyennes
        df_reso_moy = ts.resample(code_reso).agg(['mean', 'count']) # une colonne moyenne, une colonne  nombre de points dans la fenêtre

        # définition d'un "masque" de booléens par une condition
        if reso=="jour":
            invalid = df_reso_moy['count'] <=  nb_minutes_attendues_par_jour
        elif reso=="mois":
            invalid = df_reso_moy['count'] <= df_reso_moy.index.days_in_month * nb_minutes_attendues_par_jour
        else:
            invalid = df_reso_moy['count'] <= 365 * nb_minutes_attendues_par_jour

        # restrict to the sum and null out invalid entries, using invalid as a mask
        # https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html#returning-a-view-versus-a-copy
        df_reso_moy.loc[invalid, 'mean'] = np.nan
        ts_reso_moy = df_reso_moy['mean']
        print(f"Rééchantillonnage par moyenne sur pas de temps {reso}, première date = {ts_reso_moy.index[0]}")
        axe_parasite.plot(ts_reso_moy.index.shift(1, freq='D').to_pydatetime(), ts_reso_moy.to_numpy(), c=dico_resolutions_couleurs[reso],
                        marker='*', ls="None")
        axe_parasite.step(df_reso_moy.index.shift(1, freq='D').to_pydatetime(), ts_reso_moy.to_numpy(), c=dico_resolutions_couleurs[reso],
                        label=reso)  # , where='post')

        #  vignette du bas, max

        ts_reso_max = ts.resample(code_reso).max()
        ts_reso_max[invalid] = np.nan
        print(f"Rééchantillonnage par max sur pas de temps {'jour'}, première date = {ts_reso_max[0]}")
        ax_unique.plot(ts_reso_max.index.shift(1, freq='D').to_pydatetime(), ts_reso_max.to_numpy(), c=dico_resolutions_couleurs[reso],
               marker='*', ls="None")
        ax_unique.step(ts_reso_max.index.shift(1, freq='D').to_pydatetime(), ts_reso_max.to_numpy(), c=dico_resolutions_couleurs[reso])


    host_original.xaxis.set_major_locator(locator)
    host_original.xaxis.set_major_formatter(formatter)

    fig.legend(loc='lower center', ncol=4)

    host_original.format_ydata = un_chiffre_apres_la_virgule

    plt.show()

def get_month_end(dt):
    first_of_month = datetime(dt.year, dt.month, 1)
    next_month_date = first_of_month + timedelta(days=32)
    new_dt = datetime(next_month_date.year, next_month_date.month, 1)
    return new_dt - timedelta(days=1)

def dernier_jour_du_mois(dt):
    first_of_month = datetime(dt.year, dt.month, 1)
    next_month_date = first_of_month + timedelta(days=32)
    new_dt = datetime(next_month_date.year, next_month_date.month, 1)
    return (new_dt - timedelta(days=1)).day

# moyennes par MOIS   avec des rectangles
def trace_mensuel_avec_rectangles(ts_original, trace_journalier=False, trace_rect_mensuels=False):
    """
    test ici : Rectangles, axline et hlines
    """
    debut = datetime.now()

    ts_mensuel= ts_original.resample("1M").sum()
    ts_journalier = ts_original.resample("1D").sum()

    fig, ax1 = plt.subplots(figsize=(7, 4))
    fig.suptitle("chroniques_mois_rectangles")

    ax1.plot(ts_original.index.to_pydatetime(), ts_original.to_numpy(), label="lames d'eau à 5 min",
                     color='purple', zorder=10)
    ax1.set(ylabel="Lames d'eau à 5 min")

    (handles1, labels1) = plt.gca().get_legend_handles_labels()


    # pas possible de mettre un 2e axe rien qu'avec des rectangles... DONC il faut mettre une ligne "guide" avec un plot
    ax2 = ax1.twinx()
    ax2.axhline(ts_mensuel.mean(), color="red", linestyle="--", label="moyenne des moyennes mensuelles")

    ax2.set(ylabel="cumuls sur la durée (1 j ou 1 mois)")

    if trace_rect_mensuels:
        print("tracé des cumuls mensuels par des rectangles")
        for date_debut_rect, valeur in zip(ts_mensuel.index.to_pydatetime(), ts_mensuel):
            if date_debut_rect.month in [12, 1, 2]:
                code_couleur = 'blue'
            elif date_debut_rect.month in [3, 4, 5]:
                code_couleur = 'pink'
            elif date_debut_rect.month in [6, 7, 8]:
                code_couleur = 'yellow'
            else:
                code_couleur = 'orange'

            nb_jours_du_mois = dernier_jour_du_mois(date_debut_rect)
            print(date_debut_rect, valeur, code_couleur, nb_jours_du_mois)
            # patch = patches.Rectangle(xy, w, h, linewidth=1, edgecolor=edgecolor, facecolor=facecolor)
            ax2.add_patch(Rectangle((date_debut_rect+timedelta(days=1), 0.1), -timedelta(days=nb_jours_du_mois), valeur, color=code_couleur, alpha=0.8))

    if trace_journalier:
        print("tracé des cumuls journaliers par des verticales")
        ax2.vlines(x=ts_journalier.index.to_pydatetime(), ymin=0, ymax=ts_journalier.to_numpy(), color="blue", linestyle="--", label="moyennes journalières")

    trace_hlines = False
    if trace_hlines:
        print("tracé des cumuls mensuels par des segments horizontaux")
        ax2.hlines(y=ts_mensuel, xmax=ts_mensuel.index,
                   xmin=[date - timedelta(days=30) for date in ts_mensuel.index.to_pydatetime()],
                   label="cumuls mensuels", lw=3, alpha=0.8)  #  color=codes_couleur,

    (handles2, labels2) = plt.gca().get_legend_handles_labels()


    # Legende commune
    plt.gca().legend(handles1+handles2, labels1+labels2)

    #fig.legend(loc=7)

    ax1.set_ylim(0, ts_original.max() * 1.1)
    ax2.set_ylim(0, max(ts_mensuel.max(), ts_journalier.max()) * 1.1)

    # jours : Memory Error avec vlines ?

    temps = datetime.now() - debut
    plt.show()
    print("temps écoulé Pandas avec mois en rectangles", temps)
    # plt.close(fig)


def HautBas_trace_mensuel_avec_rectangles(ts_original, agg_par_cumul=True):
    """
    Avec des imperfections à corriger :
    * étiquettes de dates : ajouter ConciseDateFormatter
    * étiquettes des axes des y
    """
    debut = datetime.now()
    if agg_par_cumul:
        ts_mensuel= ts_original.resample("1M").sum()
        ts_journalier = ts_original.resample("1D").sum()
    else:
        ts_mensuel = ts_original.resample("1M").mean()
        ts_journalier = ts_original.resample("1D").mean()

    # d'après cheatsheet pbpython
    fig, (ax_agg, ax_original) = plt.subplots(nrows=2, ncols=1, sharex=True, figsize=(7, 4))
    if agg_par_cumul:
        fig.suptitle("Lames d'eau à 5 min (bas) et cumuls journaliers et mensuels (haut)")
        ax_agg.set(ylabel="cumuls")
        ax_original.set(ylabel="lame d'eau \n à 5 min")
        etiquette="lames d'eau à 5 min"
    else:
        fig.suptitle("Débit (bas) et débits moyens journaliers et mensuels (haut)")
        ax_agg.set(ylabel="moyennes")
        ax_original.set(ylabel="débit")
        etiquette = "débit"

    ax_original.plot(ts_original.index.to_pydatetime(), ts_original.to_numpy(), label=etiquette,
             color='purple', zorder=10)
    plt.legend()

    ax_agg.axhline(ts_mensuel.mean(), color="red", linestyle="--", label="moyenne des moyennes mensuelles")

    # TESTS : on enlève les rectangles
    for date_debut_rect, valeur in zip(ts_mensuel.index, ts_mensuel):
        if date_debut_rect.month in [12, 1, 2]:
            code_couleur = 'blue'
        elif date_debut_rect.month in [3, 4, 5]:
            code_couleur = 'pink'
        elif date_debut_rect.month in [6, 7, 8]:
            code_couleur = 'yellow'
        else:
            code_couleur = 'orange'

        nb_jours_du_mois = dernier_jour_du_mois(date_debut_rect)
        print(date_debut_rect, valeur, code_couleur)
        ax_agg.add_patch(Rectangle((date_debut_rect+timedelta(days=1), 0.1), -timedelta(days=nb_jours_du_mois), valeur, color=code_couleur, alpha=0.8))

    ax_j = ax_agg.twinx()
    ax_j.vlines(x=ts_journalier.index, ymin=0, ymax=ts_journalier, label="cumuls journaliers (vlines)",
                    color='red', lw=1, alpha=0.8)

    fig.legend()

    # max_valeur = max(df_lamedeau["lames d'eau en mm"].max(), df_mensuel["lames d'eau en mm"].max())
    # min_valeur = min(0, df_lamedeau["lames d'eau en mm"].min(), df_mensuel["lames d'eau en mm"].min())
    # ax1.set_ylim(min_valeur, df_mensuel["lames d'eau en mm"].max()*1.1)
    # ax1.set_ylim(min_valeur, max_valeur * 1.1)

    ax_original.set_ylim(0, ts_original.max() * 1.1)
    ax_agg.set_ylim(0, ts_mensuel.max() * 1.1)
    ax_j.set_ylim(0, ts_journalier.max() * 1.1)


    temps = datetime.now() - debut
    plt.show()
    print("temps écoulé Pandas avec mois en rectangles", temps)
    # plt.clf()


if __name__ == '__main__':

    # Premières manipulation, plot simple
    premier_atelier = False
    if premier_atelier:
        # définition d'un chemin, méthode filedialog
        chemin_complet = askopenfilename()
        if Path.is_file(Path(chemin_complet)):
            ts = lecteur_qj0_to_ts(chemin_complet)
        else:
            print("chemin ", chemin_complet, " non valide")

    # Deuxième atelier, figure avec plusieurs courbes, 2e axe des y

    deuxieme_atelier = True
    if deuxieme_atelier:
        chemin_complet = askopenfilename()
        # donnees_TD_ETP_csv(chemin_complet)
        print("Dans un premier temps, on s'en remet entièrement à pandas")
        donnees_TD_ETP_csv_panda_seul(chemin_complet)
        print("Fermez la fenêtre Tk ; on continue")

        # on peut récupérer la DataFrame ou pas...
        DF_PTQ = donnees_TD_ETP_2subplots(chemin_complet)

        # mais par exemple on en a besoin pour cette variante
        donnees_TD_ETP_2subplots_variante(DF_PTQ)
        donnees_TD_ETP_2subplots_variante2(DF_PTQ)

    quatrieme_atelier = False
    binaire_chemin_complet = askopenfilename()
    if Path.is_file(Path(binaire_chemin_complet)):
        # CHRONIQUES
        # lire_lame_deau_radar_5min(binaire_chemin_complet)    # trace la figure mais on ne récupère pas les objets retournés
        dates, valeurs = lire_lame_deau_radar_5min(binaire_chemin_complet)    # trace la figure et on récupère les objets retournés
        # tracer_proprement_une_longue_serie(dates, valeurs, info_chroniques=" série temporelle")  # donc on peut les utiliser
        # tracer_une_longue_serie_en3_resolutions_mieux(dates, valeurs, info_chroniques=" lame d'eau radar à 5 min")
        tracer_une_longue_serie_lame_deau_en3_resolutions_verifs(dates, valeurs, info_chroniques=" lame d'eau radar à 5 min")
        #trace_mensuel_avec_rectangles(pd.Series(valeurs, index=dates), trace_journalier=True, trace_rect_mensuels=True)
        #HautBas_trace_mensuel_avec_rectangles(pd.Series(valeurs, index=dates))

        # CARTES
        

