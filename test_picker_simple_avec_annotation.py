
"""
Atelier Matplotlib, C. Poulard, sept 2021
 code picker simple, calcul des distances en unités "des données"
 
 Ref : # https://matplotlib.org/stable/users/event_handling.html#object-picking
"""



import numpy as np
import matplotlib.pyplot as plt

adnotacja = None

def onpick(event):
    global adnotacja
    thisline = event.artist
    print(type(thisline))
    xdata = thisline.get_xdata()
    ydata = thisline.get_ydata()
    #print("x et y data", xdata, ydata)
    
    ind = event.ind
    print("indices  de l'event ??", ind)
    # print("position  de la souris sur le canevas", event.mouseevent.x, event.mouseevent.y)
    print("position  de la souris ! ! ", event.mouseevent.xdata, event.mouseevent.ydata)
    
    points = np.array([(xdata[i], ydata[i]) for i in ind])
    pos_souris = [(event.mouseevent.xdata, event.mouseevent.ydata)]
    print('onpick points:', points)

    distances = np.linalg.norm(points - pos_souris, axis=1)
    dataidx = np.argmin(distances)
    datapos = points[dataidx,:]
    
    print('point le plus proche:', datapos)
    xpos = event.mouseevent.xdata
    ypos = event.mouseevent.ydata

    etiq = thisline.get_label()
    print(etiq)

    xpt, ypt = datapos
    adnotacja.set_text ( f"{etiq} {points[dataidx, 0]:.2f}")
    adnotacja.set_position( (xpt+20, ypt+0.5))
    adnotacja.xy = (xpt, ypt)
    adnotacja.set_color(thisline.get_color())
    adnotacja.set_visible(True)


    
    """
    del(adnotacja)
    adnotacja = ax.annotate(f"{etiq} {points[dataidx,0]:.2f}, {points[dataidx,1]:.2f}", xytext=(xpos+2, ypos+0.1), xy=(xpos, ypos),
                color = thisline.get_color(), bbox = dict(
                boxstyle='round,pad=0.5', fc='yellow', alpha=0.75),
                arrowprops = dict(arrowstyle='->', connectionstyle='arc3,rad=0'))
    """
    plt.draw()
     

fig, ax = plt.subplots()
ax.set_title('click on points')
x_sc = [-240.838,-80.39,-78.15,-34,-20.91,-17.27,0,4.51,6.76,8.76,14.76,42.76,46.76,57.76,74.76,78.62,83.32,86.85,104,136.22,143.85,262.877]
z_sc=[142.75,139,140,139.17,139.17,138.47,137.8,136.73,135.21,135.01,134.71,134.51,134.61,134.29,134.71,135.83,138.48,137.77,139.53,140.68,141.5,141.91]
line_pk6700, = ax.plot(x_sc, z_sc, '*', c='red', label="rouge", picker=100)
                # picker=False,   # 5 points tolerance pickradius=5,

#line, = ax.plot(np.random.rand(100), 'o', label="bleue",
#               picker=True, pickradius=5)  # 5 points tolerance

minimum_des_x = min(line_pk6700.get_xdata())
minimum_des_y = min(line_pk6700.get_ydata())



adnotacja = ax.annotate("X", xytext=(minimum_des_x,minimum_des_y), xy=(minimum_des_x, minimum_des_y), bbox = dict(boxstyle='round,pad=0.5', fc='yellow', alpha=0.75), arrowprops = dict(arrowstyle='->', connectionstyle='arc3,rad=0', color='grey'))
adnotacja.set_visible(False)
fig.canvas.mpl_connect('pick_event', onpick)
plt.show()

