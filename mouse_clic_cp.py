"""
Code pour l'atelier "clics", événement de type MouseEvent
Christine Poulard, Sylvain Coulibaly, équipes hydrologie et hydraulique, Sept 2021
"""

import matplotlib.pyplot as plt
import numpy as np

# initialisation
fig = None

# fonction

def onclick_plus_proche(event):
    x_souris, y_souris = event.xdata, event.ydata
    # line2D.get_xdata() et get_ydata() permettent de récupérer les vecteurs des x et y respectivement d'un objet line2D
    # zip permet de boucler sur deux listes en même temps pour former un tuple (x,y)
    distances = [((x-x_souris)**2 + (y-y_souris)**2)**(1/2) for (x,y) in zip(donnees.get_xdata(), donnees.get_ydata())] 
    
    distance_min = min(distances)
    idx_min = np.argmin(distances)
    # on va affecter à la courbe nommée selection les coordonnées du point le plus proche
    selection.set_data([donnees.get_xdata()[idx_min]], [donnees.get_ydata()[idx_min]])
    fig.canvas.draw_idle()

# Corps du programme

fig,ax = plt.subplots()
ax.set_title("détermination du point de la courbe le plus proche")
# la méthode plot renvoie une liste d'objets line2D ;  
# on récupère le premier pour le nommer avec la syntaxe d'unpacking des tuples :    premier_terme, = tuple
donnees, = ax.plot(range(10), marker='o')       # y= les entiers de 0 à 9 ; en l'absence de x , le rang sera utilisé
selection, = ax.plot([0],[0], marker='*', c='red', markersize=20)  # un seul point, placé en (0,0) 
                 
fig.canvas.mpl_connect('button_press_event', onclick_plus_proche)
plt.show()
