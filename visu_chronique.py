# Christine Poulard, INRAE, 02/03/2021
# tests sur les widgets et les modifications dynamiques de courbes, matplotlb

from datetime import datetime, timedelta
from matplotlib import pyplot as plt
from matplotlib.dates import num2date, date2num
from matplotlib.widgets import RadioButtons, CheckButtons, Button, TextBox
import matplotlib.gridspec as gridspec
import matplotlib.dates as mdates
from matplotlib.patches import Rectangle
from matplotlib.colors import to_rgba
from matplotlib.lines import Line2D
from matplotlib import rcParams
import pandas as pd
import tkinter as Tk



#ressources :
#    https://matplotlib.org/stable/gallery/widgets/radio_buttons.html#sphx-glr-gallery-widgets-radio-buttons-py
#    https://matplotlib.org/stable/gallery/event_handling/viewlims.html
#    https://stackoverflow.com/questions/8010549/subplots-with-dates-on-the-x-axis
# https://matplotlib.org/3.3.4/gallery/event_handling/viewlims.html#sphx-glr-gallery-event-handling-viewlims-py
# https://stackoverflow.com/questions/13583153/how-to-zoomed-a-portion-of-image-and-insert-in-the-same-plot-in-matplotlib

def qj0_to_ts(nom_fichier, trace=True):

    dico_qj = dict()
    with open(nom_fichier, 'r') as fichier:
        code_station = None
        meme_station = True
        for ligne in fichier:
            if ligne[0:3] == "950":
                vecteur = ligne.strip().split(";")
                code_station = vecteur[1].strip()
                nom_station = vecteur[2].strip()
            elif "QJO" in ligne:
                vecteur = ligne.strip().split(";")
                if not code_station:
                    code_station = vecteur[1].strip()

                if vecteur[1].strip() == code_station:
                    date_amj = vecteur[2].strip()
                    annee = date_amj[0:4]
                    mois = date_amj[4:6]
                    jour = date_amj[6:8]
                    debit_lu = vecteur[3].strip()
                    date = datetime(int(annee), int(mois), int(jour))
                    dico_qj[date] = float(debit_lu) / 1000
                else:
                    meme_station = False

    print(f"Lecture terminée, de {min(dico_qj.keys())} à {max(dico_qj.keys())} ")
    print(f" {len(dico_qj)} valeurs lues, de  {min(dico_qj.items())} à {max(dico_qj.items())} ")
    print(
            f" Code station =  {code_station} , nom = {nom_station}, test une seule station dans le fichier : {meme_station} ")

    qj_series = pd.Series(dico_qj, name=code_station + ' (QJO)').resample("1D").asfreq()
    qj_series.sort_index(inplace=True)

    if trace:
        plt.ion()
        fig, ax = plt.subplots(figsize=(10, 5))

        qj_series.plot(drawstyle="steps-post")
        ax.set_ylabel("QJ, débit moyen journalier (m3/s)")

        fig.legend()
        ax.set_title(
            "lecture des fichiers qj de " + nom_station + " (imports Hydro2 qtfix)")  # , répertoire " + str(repertoire))
        # plt.setp(ax.get_xticklabels(), rotation=-30, ha="left")
        # plt.show(block=False)
        fig.canvas.draw()
        plt.ioff()

    return qj_series

# =================================
# https://stackoverflow.com/questions/16705452/matplotlib-forcing-pan-zoom-to-constrain-to-x-axes
import types

def constrainXPanZoomBehavior(fig):
    # make sure all figures' toolbars of this class call a postPressZoomHandler()
    def overrideZoomMode(oldZoom, target):
        def newZoom(self, event):
            oldZoom(self, event)
            if hasattr(self, 'postPressZoomHandler'):
                self.postPressZoomHandler()
        return newZoom
    def overrideToolbarZoom(fig, methodname, functransform, *args):
        toolbar = fig.canvas.toolbar
        oldMethod = getattr(toolbar.__class__, methodname)
        newMethod = functransform(oldMethod, toolbar, *args)
        setattr(toolbar.__class__, methodname, newMethod)
    overrideToolbarZoom(fig, 'press_zoom', overrideZoomMode)

    # for this specific instance, override the zoom mode to 'x' always
    def postPressZoomHandler(self):
        self._zoom_mode = 'x'
    fig.canvas.toolbar.postPressZoomHandler = types.MethodType(postPressZoomHandler, fig.canvas.toolbar)
    return fig

# https://matplotlib.org/3.3.4/gallery/event_handling/viewlims.html#sphx-glr-gallery-event-handling-viewlims-py
# We just subclass Rectangle so that it can be called with an Axes
# instance, causing the rectangle to update its shape to match the
# bounds of the Axes
class UpdatingRect(Rectangle):
    def __call__(self, ax):
        ax.add_patch(self)
        self.set_bounds(*ax.viewLim.bounds)
        print(" **  creatin rectangle  ** ")
        ax.figure.canvas.draw_idle()
        self.ax = ax

    def update(self, xstart, xend):
        self.set_x(xstart)
        self.set_width(xend-xstart)
        self.set_visible(True)
        print(" **  updating rectangle  ** ")
        self.ax.figure.canvas.draw_idle()


# firsts=[]
# for i in range(dstart.month, datenow.month+1):
#     firsts.append(datetime.datetime(2015,i,1))
# plt.xticks(firsts)

#https://matplotlib.org/stable/gallery/event_handling/resample.html
# A class that will downsample the data and recompute when zoomed.
class DataDisplayDownsampler:
    def __init__(self, pdseries, rect = None):
        self.pdseries = pdseries.tz_localize(None)
        self.nb_points_1 = len(pdseries)
        self.premiere_fois = True
        self.line = None
        self.rect = rect

        self.moy10j = pdseries.resample("10D", ).mean().rename("moyenne sur 10 jours")
        self.nb_points_10 = len(self.moy10j)
        self.max10j = pdseries.resample("10D").max().rename("max sur 10 jours")
        print("date ", ts.index[0], type(ts.index[0]) )
        print("date to_numpy ", type(ts.index.to_numpy()))
        print("date pd.to_pydatetime", ts.index[0].to_pydatetime(), type(ts.index[0].to_pydatetime()))
        self.date_debut_absolu = ts.index[0].to_pydatetime()   # )
        #self.date
        self.delta = ts.index[-1] - ts.index[0]
        self.type_moyenne = 1
        self.val_max = pdseries.max()

    def sous_serie_resamplee(self, xstart, xend):
        # get the points in the view range
        # mask = (self.origXData > xstart) & (self.origXData < xend)
        # dilate the mask by one to catch the points just outside
        # of the view range to not truncate the line
        #mask = np.convolve([1, 1], mask, mode='same').astype(bool)
        # sort out how many points to drop

        # downsample data
        date_debut, date_fin = num2date(xstart),num2date(xend)
        print("dates début et fin après num2date :", date_debut, date_fin, type(date_debut) )

        # on teste l'étendue
        if (date_fin - date_debut) > timedelta(days=720):
            type_moyenne =100
            self.line.set_color("green")
            subseries = self.moy10j
        else:
            type_moyenne = 1
            self.line.set_color("red")
            subseries = self.pdseries

        print("dates début et fin: ", date_debut, date_fin , type(date_debut))
        print("type : ", type_moyenne)

        # on restreint
        print("dates début et fin après correction : ", date_debut, date_fin, type(date_debut))
        date_pd_debut = pd.to_datetime(date_debut - timedelta(days=1)).tz_localize(None)
        date_pd_fin = pd.to_datetime(date_fin + timedelta(days=1)).tz_localize(None)
        subseries=subseries.loc[(date_pd_debut  <= subseries.index) & (subseries.index <= date_pd_fin)]

        # on met à jour
        print(f"Résolution {subseries.name}, {len(subseries)} parmi {self.nb_points_1} ")
        print(f"fenêtre de {subseries.index[0]} à  {subseries.index[-1]}, type: {type(subseries.index[0])}")
        print("y début et y fin=", subseries.min(), subseries.max())

        return subseries

    """
    def update(self, ax):
        # Update the line
        lims = ax.viewLim

        #if abs(lims.width - self.delta) > 1e-8:
        #    self.delta = lims.width
        xstart, xend = lims.intervalx
        ymin, ymax = lims.intervaly
        date_start, date_end = num2date(ax.get_xlim())
        print("xstart et xend = lims.intervalsx", xstart, xend , type(xend))
        print("datestart et dateend = après num2date", date_start, date_end, type(date_start))
        self.line.set_data(*self.sous_serie_resamplee(xstart, xend))

        ax.figure.canvas.draw_idle()
    """
    """
    def update2(self, ax):
        # Update the line
        lims = ax.viewLim
        ax.set_autoscale_on(False)  # Otherwise, infinite loop

        #if abs(lims.width - self.delta) > 1e-8:
        #    self.delta = lims.width
        xstart, xend = lims.intervalx
        date_start, date_end = num2date(ax.get_xlim())
        print("xstart et xend = lims.intervalsx", xstart, xend , type(xend))
        print("datestart et dateend = après num2date", date_start, date_end, type(date_start))
        subseries = self.sous_serie_resamplee(xstart, xend)
        self.line.set_data([date2num(date) for date in subseries.index], subseries.to_numpy())

        print("valeurs la plus basse et la plus haute :", subseries.min(), subseries.max())
        if self.rect is not None:
            self.rect.update(xstart,xend )

        ax.figure.canvas.draw_idle()
    """
    def update_via_radio(self, ax):
        # n'utilise plus "resample" mais les radioboutons
        lims = ax.viewLim
        ax.set_autoscale_on(False)  # Otherwise, infinite loop

        xstart, xend = lims.intervalx
        date_start, date_end = num2date(ax.get_xlim())
        print("xstart et xend = lims.intervalsx", xstart, xend , type(xend))
        print("datestart et dateend = après num2date", date_start, date_end, type(date_start))

        date_debut, date_fin = num2date(xstart), num2date(xend)
        print("dates début et fin après num2date :", date_debut, date_fin, type(date_debut))

        # on teste l'étendue
        if (date_fin - date_debut) > timedelta(days=720):
            self.radio.set_active(1)
        else:
            self.radio.set_active(0)

        if self.rect is not None:
            self.rect.update(xstart, xend)

        ax.figure.canvas.draw_idle()


def graphe_avec_pretraitement(tserie:pd.Series, nature:str) :
    plt.ion()
    fig, ax = plt.subplots(figsize=(10, 5))

    tserie.plot(drawstyle="steps-post")
    ax.set_ylabel("QJ, débit moyen journalier (m3/s)")

    fig.legend()
    ax.set_title(
        "lecture des fichiers qj de " + nom_station + " (imports Hydro2 qtfix)")  # , répertoire " + str(repertoire))
    # plt.setp(ax.get_xticklabels(), rotation=-30, ha="left")
    # plt.show(block=False)
    fig.canvas.draw()
    plt.ioff()

# https://matplotlib.org/stable/gallery/widgets/check_buttons.html
class ListBoxCourbes:
    dico_legende = {'QJ': ('1D','blue'), 'QM': ('1M', 'green'), 'QA': ('1Y', 'brown')}

    def __init__(self, ts, rax, ax):
        self.ts = ts
        self.ax = ax
        self.rax = rax
        self.lines = []
        self.rect = None
        self.initialisation()
        self.date_min = ts.index[0]
        self.date_max = ts.index[-1]
        self.val_max = ts.max()


    def restrictions_eventuelles(self, restriction):
        #en chantier, pour info (non débugué)
        derniere_date = pd.to_pydate(ts.index[-1])
        if restriction == 'QM':
            if derniere_date.month != (derniere_date + timedelta(days=1)).month:
                ts_fin = ts.index[-1]
            elif ts.index[0].month != 1:
                ts_fin = pd.to_datetime(f"{ts.index[0].year:.04d}{ts.index[0].month-1:.02d}01", format='%Y%m%d')
            else:
                ts_fin = pd.to_datetime(f"{ts.index[0].year-1:.04d}3112", format='%Y%m%d')

        elif restriction == 'QA':
            if derniere_date.year != (derniere_date + timedelta(days=1)).year:
                ts_fin = ts.index[-1]
            else :
                ts_fin = pd.to_datetime(f"{ts.index[0].year-1:.04d}3112", format='%Y%m%d')

        return ts_fin

    def initialisation(self):
        for resolution, proprietes in ListBoxCourbes.dico_legende.items():
            print (resolution, proprietes)
            (code_resample, couleur) = proprietes

            # parceque la résolution "de base" est QJ
            if resolution == "QJ" :
                ts_moy = ts
                where = 'post'
            else:
                # todo : restreindre les limites pour que les moyennes aient un sens ! ! !
                ts_moy = self.ts.resample(code_resample, closed='left', label='right').mean().rename(resolution)
                where = 'pre'

            line, = self.ax.step(ts_moy.index, ts_moy.to_numpy(), marker='None', ls='-', where=where, c=couleur, label=resolution)
            self.lines.append(line)

        self.rect = UpdatingRect([0, 0], 0, 0, facecolor=to_rgba('thistle', 0.8), edgecolor='black', linewidth=1.0)
        self.rect.set_visible(False)
        self.rect(self.ax)  # méthode __call__

        self.labels = [str(line.get_label()) for line in self.lines]
        visibility = [line.get_visible() for line in self.lines]

        self.cbox = CheckButtons(self.rax, self.labels, visibility)
        self.cbox.on_clicked(self.changer_selection)

    def changer_selection(self, label):
        index = self.labels.index(label)
        self.lines[index].set_visible(not self.lines[index].get_visible())
        self.ax.figure.canvas.draw_idle()


class BoutonRedrawHaut:

    def __init__(self, rax, ax, date_min, date_max, val_max):
        self.rax = rax
        self.ax = ax
        self.date_min = date_min
        self.date_max = date_max
        self.val_max = val_max

    def redimensionner(self, event):
        self.ax.set_xlim(self.date_min, self.date_max )
        self.ax.set_ylim(0, self.val_max*1.05)
        self.ax.figure.canvas.draw_idle()


class ResampleSelonResolution:
    dico_type ={'QJ': ('1D','blue'), 'Q10j':('10D','orange'),'QM': ('1M','green'), 'QA': ('1Y', 'brown')}

    def __init__(self, ts, ax, line):
        self.ts = ts
        self.ax = ax
        self.line = line


    def changer_courbe(self, resolution):
        code_resample, couleur = ResampleSelonResolution.dico_type[resolution]

        # ici on recalcule à chaque fois, bof
        ts_moy = self.ts.resample(code_resample).mean().rename(resolution)
        # pour mémoire: il est possible de changer la couleur du cercle, mais ça sera pris en compte au clic SUIVANT !
        # self.radio.activecolor= couleur
        self.line.set_color(couleur)
        self.line.set_xdata(ts_moy.index)
        self.line.set_ydata(ts_moy.to_numpy())
        valeur_max = ts_moy.max()
        self.ax.set_ylim(bottom=0, top=valeur_max * 1.1)
        self.ax.figure.canvas.draw_idle()


def figures_doubles(ts:pd.Series, titre="Outil de visualisation à plusieurs résolutions"):
    ts.rename("QJ")
    # ts_an = ts.resample('1Y').mean().rename("moyenne annuelle")

    d = DataDisplayDownsampler(ts)

    fig2 = plt.figure()
    constrainXPanZoomBehavior(fig2)
    fig2.suptitle(titre)
    gs = fig2.add_gridspec(3, 2, width_ratios=[1,4], height_ratios=[4, 1, 4])
    locator_h = mdates.AutoDateLocator(minticks=3, maxticks=12)
    formatter_h = mdates.ConciseDateFormatter(locator_h)
    locator_b = mdates.AutoDateLocator(minticks=3, maxticks=12)
    formatter_b = mdates.ConciseDateFormatter(locator_b)

    axcolor = 'lightgoldenrodyellow'

    # haut : "chronique entière", liste de checkboxes
    ax_haut = fig2.add_subplot(gs[0, 1])
    ax_haut.xaxis.set_major_locator(locator_h)
    ax_haut.xaxis.set_major_formatter(formatter_h)
    ax_check = fig2.add_subplot(gs[0, 0])
    ax_check.set_facecolor(axcolor)
    liste_legendes_haut = ListBoxCourbes(ts, ax_check, ax_haut)

    # remplace
    # d.line, = ax_haut.step(ts.index, ts.to_numpy(), marker='None', ls='-', where='post', c='blue')
    # legendes
    ax_legende = fig2.add_subplot(gs[1, 1])
    h = [Line2D([0], [0], color=couleur, lw=2) for code, couleur in ResampleSelonResolution.dico_type.values()]
    labels = list( ResampleSelonResolution.dico_type.keys())
    # h, l = ax_haut.get_legend_handles_labels()
    ax_legende.legend(h, labels, borderaxespad=0, ncol=4)
    ax_legende.axis("off")
    #ax_haut.legend()

    # milieu
    ax_bouton = fig2.add_subplot(gs[1, 0])
    ax_bouton.set_facecolor(axcolor)
    boutonRedim_haut = Button(ax_bouton, "Haut : restaurer \n l'étendue")

    # bas "zooms" et radiobuttons
    ax_bas = fig2.add_subplot(gs[2, 1])
    ax_bas.xaxis.set_major_locator(locator_b)
    ax_bas.xaxis.set_major_formatter(formatter_b)
    ax_radio = fig2.add_subplot(gs[2, 0])
    plt.subplots_adjust(left=0.05)
    ax_radio.set_facecolor(axcolor)
    radio = RadioButtons(ax_radio, ('QJ', 'Q10j','QM', 'QA'))

    # tracé de la moyenne annuelle
    # ax_bas.hlines(xmin=ts.index[0], xmax= ts.index[-1], y= ts.mean(),  ls='-',  colors='red')
    d.line, = ax_bas.step(ts.index, ts.to_numpy(), marker='None', ls='-', where='post', c='blue')
    selecteur = ResampleSelonResolution(ts, ax_bas, d.line)
    radio.on_clicked(selecteur.changer_courbe)
    d.rect = liste_legendes_haut.rect
    d.radio= radio
    init_Bouton = BoutonRedrawHaut(ax_bouton, ax_haut, ts.index[0], ts.index[-1], ts.max(skipna=True))
    boutonRedim_haut.on_clicked(init_Bouton.redimensionner)

    ax_bas.set_autoscale_on(False)  # Otherwise, infinite loop
    # ax_haut.set_autoscale_on(False)  # Otherwise, infinite loop

    # Connect for changing the view limits
    ax_bas.callbacks.connect('xlim_changed', d.update_via_radio)
    ax_bas.set_xlim(ts.index[0], ts.index[-1])

    plt.show()


# Créastion d'une pandas.Series, ici lecture d'une chronique de débit QJ

ts = qj0_to_ts("C:/WorkSpace/2020-BaO_MultiDSpa/Migrateurs/Meuse/B2220010_qj_hydro2_2019.txt")
ts.rename("QJ")

figures_doubles(ts)
"""

ts_an = ts.resample('1Y').mean().rename("moyenne annuelle")

d = DataDisplayDownsampler(ts)

fig, ax = plt.subplots()

# Hook up the line

d.line, = ax.step(ts.index, ts.to_numpy(), marker='None', ls='-', where='post')
d.line2, = ax.plot(ts.index, ts.to_numpy(), 'o-')
fig.autofmt_xdate()
ax.set_autoscale_on(False)  # Otherwise, infinite loop

# Connect for changing the view limits
ax.callbacks.connect('xlim_changed', d.update)
ax.set_xlim(ts.index[0], ts.index[-1])
plt.show()
"""