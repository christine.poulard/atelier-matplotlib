# Code pour l'atelier "clics", événement de type MouseEvent ; C. Poulard & S. Coulibaly, sept 2021

import matplotlib.pyplot as plt
import numpy as np

fig = None
adnotacja_ecran = None
adnotacja_ecran_2 = None
une_croix = None
carre_jaune=None
carre_orange=None
curseur = None


def onclick_proche_2courbes(event):
    def operations_sur_une_courbe(courbe, annotation, carre, ratio_w_sur_h = None):
        """
        # si on ne passe pas comme argument ratio_w_sur_h, sa valeur par défaut sera None : on raisonne en "distance données"
        # sinon, on passe le ratio largeur/hauteur de la vignette et on raisonne en "distance écran"
        """

        if ratio_w_sur_h is None:
            # on raisonne en "distances en unité des données"
            distances_ecran = [((x - x_souris)  ** 2 + (y - y_souris)**2) ** (1 / 2) for (x, y) in zip(courbe.get_xdata(), courbe.get_ydata())]
        else:
            # on raisonne en "unités écran", donc on norme par w/h unité données et on dénorme par le ratio w/h de la vignette     
            distances_ecran = [((ratio_w_sur_h *(x - x_souris) / delta_x ) ** 2 +
                ((y - y_souris) / delta_y)**2) ** (1 / 2) for (x, y) in zip(courbe.get_xdata(), courbe.get_ydata())]
        # print("liste des distances écran", distances_ecran)
        idx_ecran = np.argmin(distances_ecran)
        xpt, ypt = courbe.get_xdata()[idx_ecran], courbe.get_ydata()[idx_ecran]   # tuple 
        distance_min = min(distances_ecran)

        carre.set_data(xpt,ypt)

        annotation.xy = (xpt, ypt)
        annotation.set_visible(True)

        return xpt, ypt, distance_min
    

    x_souris, y_souris = event.xdata, event.ydata
    curseur.set_data(x_souris, y_souris)
    ax.set_title(f"détermination du point de la courbe le plus proche de {x_souris:.2f}, {y_souris:.2f}")

    # unités écran
    xdroite, xgauche = ax.get_xlim()
    ybas, yhaut = ax.get_ylim()
    delta_x = abs(xdroite - xgauche)
    delta_y = abs(ybas - yhaut)
    bbox = ax.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
    ratio_w_sur_h = bbox.width / bbox.height  # le graphique est "ratio_w_sur_h fois plus large que haut"
    print("delta_x, delta_y, ratio_w_sur_h)", delta_x, delta_y, ratio_w_sur_h)

    #print("type :", type(line_pk6700.get_data()), " => ", line_pk6700.get_data())


    # première courbe
    x1, y1, distance_min1 = operations_sur_une_courbe(line_pk6700, adnotacja_ecran, carre_jaune, ratio_w_sur_h)
    adnotacja_ecran.set_text ( f"courbe Pk6700 : \n {x1:.2f}, {y1:.2f}")
    adnotacja_ecran.set_position( (x1+100, y1+2.5))
    adnotacja_ecran.set_color("blue")
      
    
    # deuxième courbe
    x2, y2, distance_min2 = operations_sur_une_courbe(line_decalee, adnotacja_ecran_2, carre_orange, ratio_w_sur_h)
    adnotacja_ecran_2.set_text ( f"courbe décalée : \n {x1:.2f}, {y1:.2f}")
    adnotacja_ecran_2.set_position( (x1-250, y1+1.5))
    adnotacja_ecran_2.set_color("green")

    if distance_min2 < distance_min1 :
        selection.set_data([x2], [y2])
        selection.set_color("blue")
    else:
        selection.set_data([x1], [y1])
        selection.set_color("green")
    
    fig.canvas.draw_idle()

# CORPS DU PROGRAMME

fig,ax = plt.subplots()
ax.set_title("détermination du point de la courbe le plus proche")
x_sc = [-240.838,-80.39,-78.15,-34,-20.91,-17.27,0,4.51,6.76,8.76,14.76,42.76,46.76,57.76,74.76,78.62,83.32,86.85,104,136.22,143.85,262.877]
z_sc=[142.75,139,140,139.17,139.17,138.47,137.8,136.73,135.21,135.01,134.71,134.51,134.61,134.29,134.71,135.83,138.48,137.77,139.53,140.68,141.5,141.91]

carre_jaune,  =  ax.plot(x_sc[0], z_sc[0], 's', c='yellow',alpha=0.5, markersize =20)
line_pk6700, = ax.plot(x_sc, z_sc, '*', c='red', label="pk6700", ls='-')

x_2 = [x+25 for x in x_sc]
z_2 = [z+1 for z in z_sc]

minimum_des_x = min(min(x_sc), min(x_2))
minimum_des_y = min(min(z_sc), min(z_2))

curseur,  =  ax.plot(x_2[0], z_2[0], '+', c='blue', markersize =20)
carre_orange,  =  ax.plot(x_2[0], z_2[0], 's', c='orange',alpha=0.5, markersize =20)
line_decalee, = ax.plot(x_2, z_2, '^', c='sienna', label="pk6700 décalé", ls=":")
selection, = ax.plot(x_2[0], z_2[0], marker='*', c='red', markersize=20)

adnotacja_ecran = ax.annotate("X", xytext=(minimum_des_x,minimum_des_y), xy=(minimum_des_x, minimum_des_y), bbox = dict(boxstyle='round,pad=0.5', fc='yellow', alpha=0.75), arrowprops = dict(arrowstyle='->', connectionstyle='arc3,rad=0', color='grey'))
adnotacja_ecran.set_visible(False)
adnotacja_ecran_2 = ax.annotate("X", xytext=(minimum_des_x,minimum_des_y), xy=(minimum_des_x, minimum_des_y), bbox = dict(boxstyle='round,pad=0.5', fc='lightblue', alpha=0.75), arrowprops = dict(arrowstyle='->', connectionstyle='arc3,rad=0', color='grey'))
adnotacja_ecran_2.set_visible(False)
                 
fig.canvas.mpl_connect('button_press_event', onclick_proche_2courbes)
plt.show()
